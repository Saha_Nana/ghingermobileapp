import { Component, ViewChild } from '@angular/core';
import { MenuController, Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
// import { Keyboard } from '@ionic-native/keyboard';

//import { TabsPage } from '../pages/tabs/tabs';
import { BookMedPage } from '../pages/book-med/book-med';
import { LoginPage } from '../pages/login/login';
import { HomePage } from '../pages/home/home';
import { AboutPage } from '../pages/about/about';
import { TabsPage } from "../pages/tabs/tabs";
import { MenuPage } from '../pages/menu/menu';
import { DoctorHomePage } from '../pages/doctorhome/doctorhome';
import { doctornewappointmentTabsPage } from '../pages/doctornewappointment/doctornewappointment';
// import { PersonaldoctorserviceappointmentsPage } from "../pages/personaldoctorserviceappointments/personaldoctorserviceappointments";
import { DoctorgeneralappointmentsTabsPage } from "../pages/doctor_general_appointments/doctor_general_appointments";
import { DoctorpdsappointmentsTabsPage } from "../pages/doctor_pds_appointments/doctor_pds_appointments";
import { DocappointmentMenuPage } from "../pages/docappointmentmenu/docappointmentmenu";
import { ReferpatientlistsPage } from '../pages/referpatientlists/referpatientlists';
import { PdspatientdetailsPage } from '../pages/pdspatientdetails/pdspatientdetails';
import { PersonaldoctorserviceappointmentsDeclinePage } from '../pages/personaldoctorserviceappointmentsdecline/personaldoctorserviceappointmentsdecline';
import { Badge } from '@ionic-native/badge';
import { Storage } from '@ionic/storage';
import { DataProvider } from '../providers/data/data';
import { Network } from '@ionic-native/network';
import { ToastController, LoadingController, AlertController, ModalController } from 'ionic-angular';
import { Observable } from 'rxjs/Rx';
import { Placeorderforpatient } from '../pages/placeorderforpatient/placeorderforpatient';
import { UnattendedappointmentsPage } from "../pages/unattendedappointments/unattendedappointments";
import { PatientDetailsPage } from '../pages/patient-details/patient-details';
import { DocgeneralmedicationPage } from '../pages/docgeneralmedication/docgeneralmedication';
import { DocgeneralmedicationlistsPage } from '../pages/docgeneralmedicationlists/docgeneralmedicationlists';
import { AppVersion } from '@ionic-native/app-version';
import { Market } from '@ionic-native/market';
import { PhoneconsultdetailsPage } from '../pages/phoneconsultdetails/phoneconsultdetails';
import { ProfessionalInfoPage } from '../pages/professionalinfo/professionalinfo';
import { version } from 'punycode';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { UnattendedappointmentdetailsPage } from '../pages/unattendedappointmentdetails/unattendedappointmentdetails';
// import { DocgeneralmedicationPage } from "../pages/docgeneralmedication/docgeneralmedication";


//import { MedAppointPage } from '../pages/med-appoint/med-appoint';

@Component({
  templateUrl: 'app.html'

})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  doctor_id: any;
  doc_new_total_count_appoint_counter: any;
  user_login_type: any;
  current_version_name: any;
    current_version_code: any;
    current_version_id: any;
    app_platform: string;
  //  rootPage:any = DocgeneralmedicationPage;
  // rootPage = DocappointmentMenuPage;
  // rootPage:any = DoctorpdsappointmentsTabsPage;

  // rootPage:any = DoctorpdsappointmentsTabsPage;
  // rootPage:any = DoctorgeneralappointmentsTabsPage;
  //  rootPage:any =  doctornewappointmentTabsPage;
  // rootPage:any = PdspatientdetailsPage;
  // rootPage = ReferpatientlistsPage;
  // rootPage:any = DoctorHomePage
  // rootPage:any = PatientDetailsPage
  // rootPage:any = UnattendedappointmentsPage
  // rootPage: any = DocgeneralmedicationlistsPage;
  // rootPage : any = PhoneconsultdetailsPage;
  // rootPage: any = LoginPage;
  rootPage: any;
  // rootPage: any = UnattendedappointmentdetailsPage;
  // rootPage: any = ProfessionalInfoPage;
  // rootPage:any = PersonaldoctorserviceappointmentsDeclinePage;

  self = this;

  pages: Array<{ title: string, component: any, icon: string }>;
  pages2: Array<{ title: string, component: any, icon: string }>;
  pages3: Array<{ title: string, component: any, icon: string }>;
  pages4: Array<{ title: string, component: any, icon: string }>;


  constructor(public menu: MenuController, public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen,public loadingCtrl:LoadingController,private market: Market
    // ,public keyboard: Keyboard
    , private badge: Badge, public storage: Storage, public data: DataProvider, private network: Network, public alertCtrl: AlertController, private appVersion: AppVersion,public toastCtrl: ToastController,private androidPermissions: AndroidPermissions
  ) {

    this.menu.enable(true);
    //  this.menu.open();
    this.initializeApp();

    
    this.storage.get('remember').then((remember) => {
      console.log("remember = " + remember);
      if (remember == true) {
        this.storage.get("user_login_type").then((user_login_type) => {
          if (user_login_type == "Doctor") {
            this.rootPage = DoctorHomePage;
          } else if (user_login_type == "Patient") {
            this.rootPage = MenuPage;
          }else{
            this.rootPage = LoginPage;
          }
        });
      }else{
        this.rootPage = LoginPage;
      }
    });


    
    

    // }

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Settings', component: HomePage, icon: "md-settings" },
      { title: 'Help', component: HomePage, icon: "md-help-circle" },
      { title: 'Sign Out', component: AboutPage, icon: "md-power" },
    ];

    // this.network.onchange().subscribe((data) => {
    //   console.log(data);
    //   this.showalertmessage_default("Ghinger", "We just got a connection but we need to wait briefly "+data);
    // });

    // let connectSubscription = this.network.onConnect().subscribe(() => {
    //   console.log('network connected!');
    //   this.showalertmessage_default("Ghinger", "We just got a connection but we need to wait briefly");
    //   // We just got a connection but we need to wait briefly
    //   // before we determine the connection type. Might need to wait.
    //   // prior to doing any api requests as well.
    //   setTimeout(() => {
    //     if (this.network.type === 'wifi') {
    //       console.log('we got a wifi connection, woohoo!');
    //       this.showalertmessage_default("Ghinger", "we got a wifi connection, woohoo");

    //     }
    //   }, 3000);
    // });

    // this.storage.get('doctor_id').then((doctor_id) => {
    //   this.doctor_id = doctor_id;
    //   console.log(' DoctorPage doctor_id = ' + doctor_id);
    //   this.showalertmessage_default("Ghinger2", "doctor ID = " + JSON.stringify(doctor_id));


    //   if (this.doctor_id) {

    //     Observable
    //       .interval(10000)
    //       .timeInterval()
    //       .flatMap(() => this.data.get_new_general_appointments_count(this.doctor_id).then((result) =>{
    //         var jsonBody = result["_body"];

    //         if (jsonBody) {
    //           jsonBody = JSON.parse(jsonBody);

    //           if (jsonBody["total_count"][0].counter) {
    //             this.doc_new_total_count_appoint_counter = jsonBody["total_count"][0].counter;
    //             this.badge.set(this.doc_new_total_count_appoint_counter);
    //           }

    //           console.log(JSON.stringify(this.doc_new_total_count_appoint_counter));
    //           this.showalertmessage_default("Ghinger2", "counter = "+JSON.stringify(this.doc_new_total_count_appoint_counter));
    //           // this.storage.set("doc_new_total_count_appoint_counter", JSON.stringify(this.doc_new_gen_appoint_counter));
    //         }
    //       }))
    //       .subscribe(data => {
    //         console.log(data);
    //         this.showalertmessage_default("Ghinger2", "data ="+data);
    //       });
    //   }

    // });


    this.get_appointments_badge();

    platform.ready().then(() => {


      // this.get_appointments_badge();

      this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.CAMERA).then(
        result => console.log('Has permission?',result.hasPermission),
        err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.CAMERA)
      );
      
      this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.CAMERA, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
      

      this.getAppName();
      this.getPackageName();
      this.getVersionNumber();
      this.getVersionCode();
  
      
      
      // this.appVersion.getAppName();
      // this.current_version_id = this.appVersion.getPackageName();
      // this.current_version_code = this.appVersion.getVersionCode();
      // this.current_version_name = this.appVersion.getVersionNumber();


      this.listenConnection();

      this.network.onConnect().subscribe(data => {
        console.log(data)
        this.displayNetworkUpdate(data.type);
      }, error => console.error(error));

      this.network.onDisconnect().subscribe(data => {
        console.log(data)
        this.displayNetworkUpdate(data.type);
      }, error => console.error(error));

    })

    // this.requestPermission();

    // this.badge.set(7);

    // this.setBadges(20);
    // this.badge.increase(1);
    // this.badge.clear();


    // this.getPackageName();

  }

  

  async getAppName(){
    const appName = await this.appVersion.getAppName();
    console.log(appName);
  }

  async getPackageName() {
    const packageName = await this.appVersion.getPackageName();
    this.current_version_id = packageName;
    console.log(packageName);
  }

  async getVersionNumber() {
    const versionNumber = await this.appVersion.getVersionNumber();
    this.current_version_name = versionNumber;
    console.log(versionNumber);
  }

  async getVersionCode() {
    const versionCode = await this.appVersion.getVersionCode();
    this.current_version_code = versionCode;
    console.log(versionCode);
  }

  // async getPackageName() {
  //   const packageName = await this.appVersion.getPackageName();
  //   console.log("packageName = "+packageName);
  // }

  displayNetworkUpdate(connectionState: string) {
    let networkType = this.network.type;

    // this.showalertmessage_default("Ghinger", `You are now ${connectionState} via ${networkType}`);
    //connectionState = offline
  }

  private listenConnection(): void {
    this.network.onDisconnect()
      .subscribe(() => {
        // this.showalertmessage_default("Ghinger", "connection");
      });
  }

  async requestPermission() {
    try {
      let hasPermission = await this.badge.hasPermission();
      console.log(hasPermission);
      if (!hasPermission) {
        let permission = await this.badge.requestPermission();
        // let permission = await this.badge.registerPermission();
        console.log(permission);
      }
    } catch (e) {
      console.error(e);
    }
  }

  async setBadges(badgeNumber: number) {
    try {
      let badges = await this.badge.set(badgeNumber);
      console.log(badges);
    } catch (e) {
      console.error(e);
    }
  }



  initializeApp() {
    this.platform.ready().then(() => {
      //  this.menu.open();
      // this.menu.enable(true);
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.


      //  this.keyboard.disableScroll(false);

      this.requestPermission();
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  ionViewDidEnter() {
    //  this.menu.enable(true);
    this.platform.ready().then(() => {
      // this.keyboard.disableScroll(false);

    });
  }

  ionViewWillLeave() {
    this.platform.ready().then(() => {
      // this.keyboard.disableScroll(false);

    });
  }

  ngOnInit(): void {

    this.get_appointments_badge();

    this.platform.ready().then(() => {
      this.listenConnection();

      this.network.onConnect().subscribe(data => {
        console.log(data)
        this.displayNetworkUpdate(data.type);
      }, error => console.error(error));

      this.network.onDisconnect().subscribe(data => {
        console.log(data)
        this.displayNetworkUpdate(data.type);
      }, error => console.error(error));

      this.getappVersion();

    })


    // this.connectionType = this.connectionToString(Connectivity.getConnectionType());
    // Connectivity.startMonitoring(connectionType => {
    //     this.zone.run(() => {
    //         this.connectionType = this.connectionToString(connectionType);
    //         console.log("this.connectionType = " + this.connectionType);
    //         setNumber("connectionType", connectionType);
    //         Toast.makeText(this.connectionType).show();
    //     });
    // });


  }

  async get_appointments_badge() {
    try {

      this.storage.get('doctor_id').then((doctor_id) => {
        this.doctor_id = doctor_id;
        // console.log(' DoctorPage doctor_id = ' + doctor_id);
        // this.showalertmessage_default("Ghinger2", "doctor ID = " + JSON.stringify(doctor_id));


        if (this.doctor_id) {

          Observable
            .interval(200000)
            .timeInterval()
            .flatMap(() => this.data.get_new_general_appointments_count(this.doctor_id).then((result) => {
              var jsonBody = result["_body"];

              if (jsonBody) {
                jsonBody = JSON.parse(jsonBody);

                // if (jsonBody["all_appointments_overall_total"]) {
                if (jsonBody["appointments_total_without_pds"]) {

                  // console.log("all_appointments_overall_total = "+jsonBody["all_appointments_overall_total"])
                  this.badge.set(jsonBody["appointments_total_without_pds"]);
                }

                // console.log(JSON.stringify(this.doc_new_total_count_appoint_counter));
                // this.showalertmessage_default("Ghinger2", "counter = "+JSON.stringify(this.doc_new_total_count_appoint_counter));
                // this.storage.set("doc_new_total_count_appoint_counter", JSON.stringify(this.doc_new_gen_appoint_counter));
              }
            }))
            .subscribe(data => {
              // console.log(data);
              // this.showalertmessage_default("Ghinger2", "data ="+data);
            });
        }

      });

    } catch (e) {
      console.log(e);
    }
  }

  // ionViewDidLoad() {
  //     this.menu.enable(true);
  //   }


  //   ionViewWillEnter(){
  //      this.menu.enable(true);
  //   }

  getappVersion() {

    if(this.platform.is('ios')){
      this.app_platform = "IOS";
    }else{
      this.app_platform = "ANDROID";
    }

    this.data.get_appversion(this.app_platform).then((data) => {

      console.log("RESULTS IS " + JSON.stringify(data));
      var body = data["_body"];
      data = JSON.parse(body);

      var desc = body["resp_desc"];
      var code = body["resp_code"];

      if (data['resp_code'] === "000") {
        if (data['id'] && data["version"]) {
          if (this.current_version_id == data['id']) {
            console.log("App ID is same. = " + data['id']);

            if (parseInt(this.current_version_code) == parseInt(data['code'])) {
              console.log("App code is same. = " + data['code']);
            } else {

              // if (this.platform.is('android')) {


                let confirm = this.alertCtrl.create({
                  title: 'Old App Version',
                  message: 'You have an older version of this app. Kindly click OK in order to update the app on playstore',
                  buttons: [
                    {
                      text: 'OK',
                      handler: () => {
                         let loader = this.loadingCtrl.create({
                          content: "Please wait...",
                          duration: 5000
                        });
            
                        loader.present();
                 
                        setTimeout(() => {
                          console.log("going to market");
                          this.market.open(this.current_version_id);
                          // utils.openUrl("market://details?id=" + data['id']);
                    
                        }, 1);
            
                        setTimeout(() => {
                          loader.dismiss();
                        }, 1);
            
                      }
                    }
                  ]
                });
                confirm.present();


                console.log("You are using Android device");
              // } 
              // else if (this.platform.is('ios')) {

              //   dialog.alert("You have an older version of this app. Kindly click OK in order to update the app on playstore").then((alres) => {
              //     // var installed = openApp("com.facebook.katana", false);
              //     // console.log("Is it installed? " + installed);
              //     // utils.openUrl("market://details?id=304878510");

              //     var openApp = require("nativescript-open-app").openApp;
              //     var utils = require("utils/utils");

              //     var installed = openApp("fb://", false);
              //     if (!installed) {
              //       // utils.openUrl("https://itunes.apple.com/us/app/facebook/id284882215?mt=8");
              //       // itunes.apple.com/app/bars/id310633997?mt=8    This is for whatsapp
              //       utils.openUrl("https://www.apple.com/lae/ios/app-store/");
              //     }
              //   });
              //   console.log("You are using IOS device")
              // }

              console.log("App code is different. = " + data['code'] + " " + this.current_version_code);
            }

          } else {
            console.log("App ID is different. = " + data['id'] + this.current_version_id);
          }




        }
      }
      console.log(data['resp_code']);
      console.log(data);
    }, (err) => {
      console.log(err);
    });

  }


  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }


  private presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 7000,
      position: 'top'
    });
    toast.present();
  }

  showalertmessage_default(titlemsg, mainmsg) {
    let alert = this.alertCtrl.create({
      title: titlemsg,
      subTitle: mainmsg,
      buttons: ['OK']
    });
    alert.present();
  }

}




// import { Component, ViewChild } from '@angular/core';
// import { SplashScreen } from '@ionic-native/splash-screen';
// import { StatusBar } from '@ionic-native/status-bar';
// // import { TranslateService } from '@ngx-translate/core';
// import { MenuController, Config, Nav, Platform } from 'ionic-angular';

// // import { FirstRunPage } from '../pages1';
// // import { Settings } from '../providers';

// // import { Keyboard } from '@ionic-native/keyboard';

// //import { TabsPage } from '../pages/tabs/tabs';
// import { BookMedPage } from '../pages/book-med/book-med';
// import { LoginPage } from '../pages/login/login';
//  import { HomePage } from '../pages/home/home';
//  import { AboutPage } from '../pages/about/about';
//   import { MenuPage } from '../pages/menu/menu';
// //import { MedAppointPage } from '../pages/med-appoint/med-appoint';



// // @Component({
// //   template: `<ion-menu [content]="content">
// //     <ion-header>
// //       <ion-toolbar>
// //         <ion-title>Pages</ion-title>
// //       </ion-toolbar>
// //     </ion-header>

// //     <ion-content>
// //       <ion-list>
// //         <button menuClose ion-item *ngFor="let p of pages" (click)="openPage(p)">
// //           {{p.title}}
// //         </button>
// //       </ion-list>
// //     </ion-content>

// //   </ion-menu>
// //   <ion-nav #content [root]="rootPage"></ion-nav>`
// // })
// @Component({
//   templateUrl: 'app.html'

// })

// export class MyApp {
//   // rootPage = LoginPage;
//   rootPage:any = LoginPage;

//   @ViewChild(Nav) nav: Nav;

//   // pages: any[] = [
//   //   { title: 'Tutorial', component: 'TutorialPage' },
//   //   { title: 'Welcome', component: 'WelcomePage' },
//   //   { title: 'Tabs', component: 'TabsPage' },
//   //   { title: 'Cards', component: 'CardsPage' },
//   //   { title: 'Content', component: 'ContentPage' },
//   //   { title: 'Login', component: 'LoginPage' },
//   //   { title: 'Signup', component: 'SignupPage' },
//   //   { title: 'Master Detail', component: 'ListMasterPage' },
//   //   { title: 'Menu', component: 'MenuPage' },
//   //   { title: 'Settings', component: 'SettingsPage' },
//   //   { title: 'Search', component: 'SearchPage' }
//   // ]
//   pages: Array<{ title: string, component: any, icon: string }>;
//   pages2: Array<{ title: string, component: any, icon: string }>;
//   pages3: Array<{ title: string, component: any, icon: string }>;
//   pages4: Array<{ title: string, component: any, icon: string }>;


//   constructor(public menu: MenuController,
//     // private translate: TranslateService,
//     public platform: Platform,
//     // settings: Settings,
//      private config: Config, private statusBar: StatusBar, private splashScreen: SplashScreen) {
//     this.menu.enable(true);
//   //  this.menu.open();
//    this.initializeApp();

//   //   // used for an example of ngFor and navigation
//   //   this.pages = [
//   //     { title: 'Settings', component: HomePage, icon: "md-settings" },
//   //     { title: 'Help', component: HomePage, icon: "md-help-circle" },
//   //      { title: 'Sign Out', component: AboutPage, icon: "md-power" },



//   //   ];

//     platform.ready().then(() => {
//       // Okay, so the platform is ready and our plugins are available.
//       // Here you can do any higher level native things you might need.
//       this.statusBar.styleDefault();
//       this.splashScreen.hide();
//     });
//     // this.initTranslate();
//   }


//   initializeApp() {
//     this.platform.ready().then(() => {
//       //  this.menu.open();
//       // this.menu.enable(true);
//       // Okay, so the platform is ready and our plugins are available.
//       // Here you can do any higher level native things you might need.


//     // this.keyboard.disableScroll(false);


//       this.statusBar.styleDefault();
//       this.splashScreen.hide();
//     });
//   }

// //   ionViewDidEnter() {
// //     //  this.menu.enable(true);
// // this.platform.ready().then(() => {
// // // this.keyboard.disableScroll(false);
// // });
// // }

// // ionViewWillLeave() {
// //   //  this.menu.enable(true);
// // this.platform.ready().then(() => {
// // // this.keyboard.disableScroll(false);
// // });
// // }

//   // initTranslate() {
//   //   // Set the default language for translation strings, and the current language.
//   //   this.translate.setDefaultLang('en');
//   //   const browserLang = this.translate.getBrowserLang();

//   //   if (browserLang) {
//   //     if (browserLang === 'zh') {
//   //       const browserCultureLang = this.translate.getBrowserCultureLang();

//   //       if (browserCultureLang.match(/-CN|CHS|Hans/i)) {
//   //         this.translate.use('zh-cmn-Hans');
//   //       } else if (browserCultureLang.match(/-TW|CHT|Hant/i)) {
//   //         this.translate.use('zh-cmn-Hant');
//   //       }
//   //     } else {
//   //       this.translate.use(this.translate.getBrowserLang());
//   //     }
//   //   } else {
//   //     this.translate.use('en'); // Set your language here
//   //   }

//   //   this.translate.get(['BACK_BUTTON_TEXT']).subscribe(values => {
//   //     this.config.set('ios', 'backButtonText', values.BACK_BUTTON_TEXT);
//   //   });
//   // }

//   openPage(page) {
//     // Reset the content nav to have just this page
//     // we wouldn't want the back button to show in this scenario
//     this.nav.setRoot(page.component);
//   }
// }
