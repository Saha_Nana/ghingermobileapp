import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { ToastController, LoadingController, AlertController, ModalController } from 'ionic-angular';
import { DataProvider } from '../../providers/data/data';
import { CompleteTestService } from '../../providers/complete-test-service/complete-test-service';
// // import { Keyboard } from '@ionic-native/keyboard';
import 'rxjs/add/operator/map';
import { MedicationdetailsPage } from "../medicationdetails/medicationdetails";
import { VidConsultPage } from "../vid-consult/vid-consult";
import { VideoconsultdetailsPage } from "../videoconsultdetails/videoconsultdetails";
import { HomeSearchPage } from "../home-search/home-search";
import { HomecaredetailsPage } from "../homecaredetails/homecaredetails";

@Component({
  selector: 'page-homecarehistory',
  templateUrl: 'homecarehistory.html',
})
export class HomecarehistoryPage {
  @ViewChild('searchbar') myInput;
  @ViewChild('input')
  // searchbar: any;

  messageList: any;
  api_code: any;
  location: any;
  displayData: any;
  check: any;
  from_menu: any = [];
  body: any;
  jsonBody: any;
  params: any = [];
  newparams: any;
  from_login: any = [];
  from_login2: any = [];
  from_login3: any = [];
  sub_id: any;
  string: any;

  requester_id1: any;
  from_login_doc: any = [];
  from_login_pers: any = [];
  body1: any;
  retrieve1: any;
  jsonBody1: any;

  body2: any;
  jsonBod2: any;
  homecare_details: any = [];
  content: any = [];
  rowid: any;
  appointmentType: any;

  constructor(
    // private keyboard: Keyboard, 
    public navCtrl: NavController, public navParams: NavParams, public completeTestService: CompleteTestService, public data: DataProvider, public loadingCtrl: LoadingController, public alertCtrl: AlertController, public modalCtrl: ModalController, public viewCtrl: ViewController) {

    // console.log("We are in Home Care Consult Appointments History page");
    this.from_login = this.navParams.get('value');

    this.from_login2 = this.navParams.get('pers_value');
    this.from_login3 = this.navParams.get('doc_value');
    this.appointmentType = navParams.get("appointmentType");
    // console.log('VALUE IN TABS CONSTRUCTOR IS' + this.from_login);

    this.body = this.from_login; // this.body = Array.of(this.from_login);

    this.jsonBody = this.body; // this.jsonBody = JSON.parse(this.body);

    this.requester_id1 = this.jsonBody[0].id;
    // this.check = this.jsonBody[0];

    this.params = {
      "requester_id": this.requester_id1,
      "appointment_type_id": this.appointmentType
    }

    this.newparams = JSON.stringify(this.params);

    console.log("New params " + this.newparams);

    this.getHomeCareAppointmentHistory();
  }

  closeModal() {
    this.viewCtrl.dismiss();
  }

  openNewAppointment() {

    this.navCtrl.push(HomeSearchPage, {
      value: this.from_login, doc_value: this.from_login3, pers_value: this.from_login2, appointmentType: this.appointmentType
    });
  }

  getHomeCareAppointmentHistory() {

    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });

    loading.present();

    setTimeout(() => {
      this.jsonBody = JSON.parse(this.newparams);

      this.data.getHomeCareAppointmentHistory(this.jsonBody)
        .then(result => {
          // this.contacts = result;
          console.log(result);

          var jsonBody = result["_body"];
          jsonBody = JSON.parse(jsonBody);
          this.homecare_details = jsonBody;

          console.log("Jsson body " + jsonBody);
          loading.dismiss();
        });

      loading.dismiss();

    }, 1);

  }


  homecare_appointment_history_details(homecare_appoint_history_id, appointmentType) {
    this.navCtrl.push(HomecaredetailsPage, { value: this.from_login, doc_value: this.from_login_doc, pers_value: this.from_login_pers, homecare_appoint_history_id: homecare_appoint_history_id, appointmentType: appointmentType });
    // rowid: rowid
  }


}
