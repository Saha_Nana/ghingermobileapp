import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { SignupPage } from '../signup/signup';
import { MenuPage } from '../menu/menu';
import { NoticePage } from '../notice/notice';
import { PasswordPage } from '../password/password';
import { ToastController, LoadingController, AlertController } from 'ionic-angular';
import { DataProvider } from '../../providers/data/data';
import { Http } from '@angular/http';
import { DoctorPage } from '../doctor/doctor';
import { FormBuilder, Validators } from '@angular/forms';
import { DoctorHomePage } from '../doctorhome/doctorhome';
import { Storage } from '@ionic/storage';
import moment from 'moment';





@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  splash = true;

  public loginForm: any;
  submitAttempt: boolean = false;
  loginVal: any;
  jsonBody: any;
  jsonBody1: any;
  messageList: any;
  api_code: any;
  Phonenumber: string;
  PIN: string;
  retrieve: string;
  retrieve1: string;
  retrieve_pers: string;
  retrieve_doc: string;
  retrieve_doc3: string;
  retrieve_user_status: any;

  searchbar: any;
  from_login: any = [];

  location: any;
  displayData: any;
  check: any;
  from_menu: any = [];
  body: any;
  body1: any;
  person_type: any;
  person_type1: any;
  doctor_id: any;
  id: any;
  doc_id: any;
  doctor_id2: any;
  doc_params: any = [];
  doc_params2: any = [];
  jsonBody3: any;
  jsonBody4: any;
  my_body: any;
  rememberme: any;

  public today = new Date().toISOString();

  // params: any = [];

  requester_id: any;
  data1: any = [];

  constructor(public toastCtrl: ToastController, public data: DataProvider, public _form: FormBuilder, public loadingCtrl: LoadingController, public alertCtrl: AlertController, public navCtrl: NavController, public navParams: NavParams, public http: Http, public storage: Storage) {

    this.storage.get('remember').then((remember) => {
      console.log("remember = " + remember);
      if (remember == true) {

        this.storage.get("user_login_type").then((user_login_type) => {
          if (user_login_type == "Doctor") {
            this.navCtrl.setRoot(DoctorHomePage);
          } else if (user_login_type == "Patient") {
            this.navCtrl.setRoot(MenuPage);
          }
        });

      }
    });

    this.loginForm = this._form.group({

      "password": ["", Validators.compose([Validators.required])],
      "username": ["", Validators.compose([Validators.required])],
      "rememberme": [false]
    })

  }

  ionViewDidLoad() {
    setTimeout(() => this.splash = false, 4000);
    console.log('ionViewDidLoad SliderPage');
  }



  params = {

    "password": "",
    "username": ""
  }




  signup() {
    this.navCtrl.push(SignupPage)
  }

  login() {
    // this.login_logic();
    this.try_login_logic();
  }

  login_logic() {

    this.loginVal = JSON.stringify(this.loginForm.value);
    console.log("LETS SEE THE LOGIN VAL " + this.loginVal)

    this.jsonBody = JSON.parse(this.loginVal);
    console.log("THIS IS THE SIGNUP VALUES" + this.jsonBody)
    console.log("THIS IS THE SIGNUP VALUES STRINGIFY" + JSON.stringify(this.jsonBody))



    let loader = this.loadingCtrl.create({
      content: "Please wait ...",
    });

    loader.present();

    if (this.jsonBody) {
      this.data.retrieve1(this.jsonBody).then((result) => {


        var body1 = result["_body"];
        body1 = JSON.parse(body1);

        this.retrieve1 = body1;
        this.retrieve1 = JSON.stringify(body1);
        console.log("##################### 1. START retrieve1 #############################");
        console.log('LETS SEE THE DATA RETRIEVED : retrieve1 ' + JSON.stringify(this.retrieve1));
        console.log("PARAMS = jsonBody " + JSON.stringify(this.jsonBody));
        console.log("####################### END retrieve1 #############################");

        this.body1 = Array.of(this.retrieve1);
        this.jsonBody1 = JSON.parse(this.body1);

        if (this.jsonBody1[0]) {

          console.log('-----------------------------------------------------');
          if (this.jsonBody1[0].user_type) {
            this.person_type1 = this.jsonBody1[0].user_type;
            console.log('VALUE of USER TYPE  IS ' + this.person_type1);
          }
          if (this.jsonBody1[0].id) {
            this.doctor_id2 = this.jsonBody1[0].id;
            console.log('VALUE of REGIS ID  IS ' + this.doctor_id2);
          }
          console.log('-----------------------------------------------------');
        }


        this.data.retrieve(this.jsonBody).then((result) => {

          var body = result["_body"];
          body = JSON.parse(body);

          this.retrieve = body
          this.retrieve = JSON.stringify(body)

          console.log("##################### 2. START retrieve #############################");
          console.log('LETS SEE THE DATA RETRIEVED : retrieve ' + JSON.stringify(this.retrieve));
          console.log("PARAMS = jsonBody " + JSON.stringify(this.jsonBody));
          console.log("####################### END retrieve #############################");

          this.body = Array.of(this.retrieve)
          this.jsonBody3 = JSON.parse(this.body);

          if (this.jsonBody3[0]) {
            if (this.jsonBody3[0].person_type) {
              this.person_type = this.jsonBody3[0].person_type;
              console.log('VALUE of PERSON TYPE  IS ' + this.person_type);
            }
          }

          this.data.retrieve_pers(this.jsonBody).then((result) => {


            var body = result["_body"];
            body = JSON.parse(body);

            this.retrieve_pers = body
            this.retrieve_pers = JSON.stringify(body)

            console.log("##################### 3. START retrieve_pers #############################");
            console.log('LETS SEE THE DATA RETRIEVED : retrieve_pers ' + JSON.stringify(this.retrieve_pers));
            console.log("PARAMS = jsonBody " + JSON.stringify(this.jsonBody));
            console.log("####################### END retrieve_pers #############################");

            this.body = Array.of(this.retrieve_pers);
            this.jsonBody4 = JSON.parse(this.body);

            if (this.jsonBody4[0]) {
              if (this.jsonBody4[0].doctor_id) {
                this.doctor_id = this.jsonBody4[0].doctor_id;

                if (this.doctor_id) {
                  console.log('VALUE of DOCTOR ID IS ' + this.doctor_id);
                  // this.storage.set('doctor_id',this.doctor_id);
                  this.doc_id = JSON.stringify(this.doctor_id);
                  this.jsonBody1 = JSON.parse(this.doc_id);
                  console.log("THIS IS THE JSON VALUES " + this.jsonBody1)
                }

              }
            }

            if (this.doctor_id2) {

              this.doc_params = {
                "id": this.doctor_id2
              }



              console.log("------------DOC PARAMS----------");
              console.log(this.doc_params);


              this.data.retrieve_doc(this.doc_params).then((result) => {

                var body = result["_body"];
                body = JSON.parse(body);

                this.retrieve_doc = body
                this.retrieve_doc = JSON.stringify(body);

                console.log("##################### 4. START retrieve_doc #############################");
                console.log('LETS SEE THE DATA RETRIEVED : retrieve_doc ' + JSON.stringify(this.retrieve_doc));
                console.log("PARAMS = doc_params " + JSON.stringify(this.doc_params));
                console.log("####################### END retrieve_doc #############################");


                if (this.doc_params2) {

                  this.doc_params2 = {
                    "id": this.doctor_id
                  }

                  this.data.retrieve_doc(this.doc_params2).then((result) => {
                    console.log(JSON.stringify(result));
                    var body = result["_body"];
                    console.log("LETS SEE BODY " + body)
                    if (body == "") {
                      console.log("BODY IS EMPTY")
                    } else {
                      body = JSON.parse(body);

                      this.retrieve_doc3 = body
                      this.retrieve_doc3 = JSON.stringify(body)
                      // console.log('LETS SEE THE DOCTOR INFO DATA RETRIEVED ' + this.retrieve_doc3);

                      console.log("##################### 5. START retrieve_doc #############################");
                      console.log('LETS SEE THE DATA RETRIEVED : retrieve_doc3 ' + JSON.stringify(this.retrieve_doc3));
                      console.log("PARAMS = doc_params2 " + JSON.stringify(this.doc_params2));
                      console.log("####################### END retrieve_doc #############################");

                    }


                    this.data.ghinger_login(this.jsonBody).then((result) => {

                      console.log(result);
                      var jsonBody = result["_body"];
                      console.log(jsonBody);

                      jsonBody = JSON.parse(jsonBody);
                      console.log(jsonBody)

                      console.log("##################### 6. START ghinger_login #############################");
                      console.log('LETS SEE THE DATA RETRIEVED : jsonBody ' + JSON.stringify(this.jsonBody));
                      console.log("PARAMS = jsonBody " + JSON.stringify(this.jsonBody));
                      console.log("####################### END ghinger_login #############################");

                      var desc = jsonBody["resp_desc"];
                      var code = jsonBody["resp_code"];


                      console.log(desc);
                      console.log(code);

                      this.messageList = desc;
                      this.api_code = code;


                      loader.dismiss();
                      if (this.api_code == "117") {
                        let loader = this.loadingCtrl.create({
                          content: "Login processing..."
                        });

                        loader.present();

                        console.log("############## this.person_type1 ##################" + this.person_type1);
                        if (this.person_type1 == "D") {

                          setTimeout(() => {
                            this.navCtrl.setRoot(
                              // DoctorPage,
                              DoctorHomePage,

                              { value: this.retrieve, doc_value: this.retrieve_doc, pers_value: this.retrieve_pers, doc_value2: this.retrieve1 });

                          }, 3000);

                          setTimeout(() => {
                            loader.dismiss();
                          }, 3000);
                        }

                        else if (this.person_type1 == "C") {
                          console.log("############## this.person_type1 ##################" + this.person_type1 + "go to menu page");
                          setTimeout(() => {
                            console.log("after login successful, we are heading to Menupage - By Padmore");
                            this.navCtrl.setRoot(MenuPage,

                              { value: this.retrieve, doc_value: this.retrieve_doc3, pers_value: this.retrieve_pers });

                          }, 3000);

                          setTimeout(() => {
                            loader.dismiss();
                          }, 3000);

                        }

                      }
                      else {
                        let alert = this.alertCtrl.create({
                          title: "",
                          subTitle: this.messageList,
                          buttons: ['OK']
                        });
                        alert.present();
                      }

                      // if (this.api_code == "201") {
                      //   let alert = this.alertCtrl.create({
                      //     title: "",
                      //     subTitle: this.messageList,
                      //     buttons: ['OK']
                      //   });
                      //   alert.present();
                      // }

                      // if (this.api_code == "118") {
                      //   let alert = this.alertCtrl.create({
                      //     title: "",
                      //     subTitle: this.messageList,
                      //     buttons: ['OK']
                      //   });
                      //   alert.present();
                      // }

                      // if (this.api_code == "103") {
                      //   let alert = this.alertCtrl.create({
                      //     title: "",
                      //     subTitle: this.messageList,
                      //     buttons: ['OK']
                      //   });
                      //   alert.present();
                      // }

                      // if (this.api_code == "222") {
                      //   let alert = this.alertCtrl.create({
                      //     title: "",
                      //     subTitle: this.messageList,
                      //     buttons: ['OK']
                      //   });
                      //   alert.present();
                      // }

                      // if (this.api_code == "001") {
                      //   let alert = this.alertCtrl.create({
                      //     title: "",
                      //     subTitle: this.messageList,
                      //     buttons: ['OK']
                      //   });
                      //   alert.present();
                      // }

                    }, (err) => {

                      let alert = this.alertCtrl.create({
                        title: "",
                        subTitle: "Sign In Unsuccessful",
                        buttons: ['OK']
                      });
                      alert.present();

                      this.toastCtrl.create({
                        message: "Please ensure that all details provided are correct and try again.",
                        duration: 5000
                      }).present();
                      loader.dismiss();
                      console.log(err);
                    });



                  });
                }



              });
            }




          });

        });

      });
    }


  }





  try_login_logic() {


    if (this.rememberme) {
      console.log("rememberme = " + this.rememberme);
      if (this.rememberme == false) {
        console.log("rememberme is false = " + this.rememberme);
        this.storage.clear();
      } else {
        console.log("rememberme is true ... setting remember = " + this.rememberme);
        this.storage.set("remember", true);
      }
    }else{
      console.log("rememberme is false = " + this.rememberme);
      this.storage.clear();
    }

    let loader = this.loadingCtrl.create({
      content: "Please wait ...",
    });

    loader.present();

    setTimeout(() => {


      this.loginVal = JSON.stringify(this.loginForm.value);
      console.log("LETS SEE THE LOGIN VAL " + this.loginVal)

      this.jsonBody = JSON.parse(this.loginVal);

      console.log("Start login request = " + moment().format('MMMM Do YYYY, h:mm:ss a') + " OR " + this.today);

      this.data.try_login(this.jsonBody).then((result) => {

        // console.log(JSON.stringify(result));
        // console.log("End login request = "+ moment().format('MMMM Do YYYY, h:mm:ss a') + " OR " +this.today);
        // var jsonBody = result["_body"];
        // console.log(jsonBody);

        // jsonBody = JSON.parse(jsonBody);
        // console.log(jsonBody)

        // this.body = Array.of(jsonBody);
        // this.jsonBody4 = JSON.parse(this.body);


        // console.log("##################### NEW. START try_login #############################");
        // console.log('LETS SEE THE DATA RETRIEVED : jsonBody ' + JSON.stringify(this.jsonBody1));
        // console.log("PARAMS = jsonBody "+JSON.stringify(this.jsonBody));
        // console.log("####################### END try_login #############################");






        var body1 = result["_body"];
        body1 = JSON.parse(body1);

        this.retrieve1 = body1;
        this.retrieve1 = JSON.stringify(body1);
        console.log("##################### 1. START retrieve1 #############################");
        console.log('LETS SEE THE DATA RETRIEVED : retrieve1 ' + JSON.stringify(this.retrieve1));
        console.log("PARAMS = jsonBody " + JSON.stringify(this.jsonBody));
        console.log("####################### END retrieve1 #############################");

        this.body1 = Array.of(this.retrieve1);
        this.jsonBody1 = JSON.parse(this.body1);

        console.log(" this.jsonBody1 = " + JSON.stringify(this.jsonBody1.resp_desc));


        if (this.jsonBody1["LOGIN_SUC"]) {

          var desc = this.jsonBody1["LOGIN_SUC"].resp_desc;
          var code = this.jsonBody1["LOGIN_SUC"].resp_code;

          console.log(desc);
          console.log(code);

          this.messageList = desc;
          this.api_code = code;


          if (this.api_code == "117") {

            let loader = this.loadingCtrl.create({
              content: "Login processing..."
            });

            loader.present();

            //process the other variables

            if (this.jsonBody1["result1"]) {
              console.log("this.jsonBody1['result1'] =  " + JSON.stringify(this.jsonBody1["result1"]));


              // this.my_body = JSON.parse(this.jsonBody1["result1"]);
              this.my_body = this.jsonBody1["result1"];

              this.my_body = JSON.stringify(this.my_body);
              console.log("jsonBody = " + this.my_body);

              this.my_body = Array.of(this.my_body)
              this.my_body = JSON.parse(this.my_body);


              if (this.my_body[0].user_type) {
                this.person_type1 = this.my_body[0].user_type;
                console.log('VALUE of USER TYPE  IS ' + this.person_type1);
              }
            }


            //result1
            if (this.jsonBody1["result1"]) {
              console.log("result 1 begin");

              this.my_body = this.jsonBody1["result1"];
              this.my_body = JSON.stringify(this.my_body);
              // console.log("jsonBody = "+this.my_body);
              // this.my_body = Array.of(this.my_body)
              this.my_body = JSON.parse(this.my_body);

              this.retrieve1 = this.my_body;

              console.log("this.jsonBody1['result1'] : retrieve1 " + JSON.stringify(this.retrieve1));


              // this.my_body = this.jsonBody1["result1"];
              // this.my_body = JSON.parse(this.my_body);
              // this.retrieve1 = this.my_body;
              // this.retrieve1 = JSON.stringify(this.my_body);

              console.log("result 1 end");

              // if(this.doctor_id){
              //   console.log('VALUE of DOCTOR ID IS ' + this.doctor_id);
              //   // this.storage.set('doctor_id',this.doctor_id);
              //   this.doc_id = JSON.stringify(this.doctor_id);
              //   this.jsonBody1 = JSON.parse(this.doc_id);
              //   console.log("THIS IS THE JSON VALUES " + this.jsonBody1)
              // }

            }

            //result2
            if (this.jsonBody1["result2"]) {

              console.log("result 2 begin");

              this.my_body = this.jsonBody1["result2"];
              this.my_body = JSON.stringify(this.my_body);
              console.log("jsonBody = " + this.my_body);
              // this.my_body = Array.of(this.my_body)
              this.my_body = JSON.parse(this.my_body);

              this.retrieve = this.my_body;

              // var body = this.jsonBody1["result2"];
              // body = JSON.parse(body);
              // this.retrieve = body;
              // this.retrieve = JSON.stringify(body);

              console.log("result 2 end");

              // this.retrieve = this.jsonBody1["result2"];
            }

            //result3
            if (this.jsonBody1["result3"]) {

              this.my_body = this.jsonBody1["result3"];
              this.my_body = JSON.stringify(this.my_body);
              console.log("jsonBody = " + this.my_body);
              // this.my_body = Array.of(this.my_body)
              this.my_body = JSON.parse(this.my_body);

              this.retrieve_pers = this.my_body;


              // var body = this.jsonBody1["result3"];
              // body = JSON.parse(body);
              // this.retrieve_pers = body;
              // this.retrieve_pers = JSON.stringify(body);

              // this.retrieve_pers = this.jsonBody1["result3"];
            }

            //result4
            if (this.jsonBody1["result4"]) {

              this.my_body = this.jsonBody1["result4"];
              this.my_body = JSON.stringify(this.my_body);
              console.log("jsonBody = " + this.my_body);
              // this.my_body = Array.of(this.my_body)
              this.my_body = JSON.parse(this.my_body);

              this.retrieve_doc = this.my_body;
              this.retrieve_doc3 = this.my_body;

              // var body = this.jsonBody1["result3"];
              // body = JSON.parse(body);
              // this.retrieve_doc = body;
              // this.retrieve_doc = JSON.stringify(body);

              // this.retrieve_doc = this.jsonBody1["result4"];
            }


            if (this.jsonBody1["retrieve_user_status"]) {

              console.log("retrieve_user_status begin");

              this.my_body = this.jsonBody1["retrieve_user_status"];
              this.my_body = JSON.stringify(this.my_body);
              console.log("jsonBody = " + this.my_body);
              this.my_body = JSON.parse(this.my_body);

              this.retrieve_user_status = this.my_body;

              console.log("retrieve_user_status end");

            }



            if (this.person_type1) {
              console.log("############## this.person_type1 ##################" + this.person_type1);
              if (this.person_type1 == "D") {

                setTimeout(() => {
                  this.storage.set("user_login_type", "Doctor");

                  this.storage.set("value", this.retrieve);
                  this.storage.set("doc_value", this.retrieve_doc);
                  this.storage.set("pers_value", this.retrieve_pers);
                  this.storage.set("doc_value2", this.retrieve1);
                  this.storage.set("retrieve_user_status", this.retrieve_user_status);


                  this.navCtrl.setRoot(DoctorHomePage,
                    { 
                      // value: this.retrieve, doc_value: this.retrieve_doc, pers_value: this.retrieve_pers, doc_value2: this.retrieve1, retrieve_user_status: this.retrieve_user_status 
                    });

                }, 1);

                setTimeout(() => {
                  loader.dismiss();
                }, 1);
              }

              else if (this.person_type1 == "C") {
                console.log("############## this.person_type1 ##################" + this.person_type1 + "go to menu page");
                setTimeout(() => {
                  this.storage.set("user_login_type", "Patient");

                  this.storage.set("value", this.retrieve);
                  this.storage.set("doc_value", this.retrieve_doc3);
                  this.storage.set("pers_value", this.retrieve_pers);
                  this.storage.set("results_1", this.retrieve1);

                  console.log("after login successful, we are heading to Menupage - By Padmore");
                  this.navCtrl.setRoot(MenuPage,
                    { 
                      // value: this.retrieve, doc_value: this.retrieve_doc3, pers_value: this.retrieve_pers, results_1: this.retrieve1 
                    });

                }, 1);

                setTimeout(() => {
                  loader.dismiss();
                }, 1);

              }
            }



          }
          else {
            let alert = this.alertCtrl.create({
              title: "",
              subTitle: this.messageList,
              buttons: ['OK']
            });
            alert.present();
          }




        }
        else {
          // var desc = this.jsonBody1["LOGIN_SUC"].resp_desc;
          // // var code = this.jsonBody1["LOGIN_SUC"].resp_code;

          // console.log(desc);
          // console.log(code);

          this.messageList = JSON.stringify(this.jsonBody1.resp_desc);
          // this.api_code = code;

          let alert = this.alertCtrl.create({
            title: "Ghinger",
            subTitle: this.messageList,
            buttons: ['OK']
          });
          alert.present();
        }



        // var desc = jsonBody["resp_desc"];
        // var code = jsonBody["resp_code"];


        // console.log(desc);
        // console.log(code);

        // this.messageList = desc;
        // this.api_code = code;


        loader.dismiss();



      }, (err) => {

        let alert = this.alertCtrl.create({
          title: "",
          subTitle: "Sign In Unsuccessful",
          // subTitle: "An Error Occured. Please try again.",
          buttons: ['OK']
        });
        alert.present();

        this.toastCtrl.create({
          message: "Please ensure that all details provided are correct and try again.",
          duration: 5000
        }).present();
        loader.dismiss();
        console.log(err);
      });

    }, 1);

  }

  check_rememberme() {
    console.log('rememberme  state:' + this.rememberme);
  }


  reset() {
    this.navCtrl.push(PasswordPage)
  }


}
