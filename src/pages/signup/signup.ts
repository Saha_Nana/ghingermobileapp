import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { ToastController, LoadingController, AlertController } from 'ionic-angular';
import { DataProvider } from '../../providers/data/data';
import { Http } from '@angular/http';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PasswordValidation } from './password-validation';
import 'rxjs/add/operator/map';
import { AddRegisPage } from '../add-regis/add-regis';
import { ProfessionalInfoPage } from '../professionalinfo/professionalinfo';




@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {


  public signupForm: any;
  submitAttempt: boolean = false;
  messageList: any;
  api_code: any;
  signupVal: any;
  jsonBody: any;
  regid: any;
  maxSelectabledate: any;
  date: any;
  countries: any;
  regions: any;
  cities: any;
  suburbs: any;

  country_id: any;

  public itemList: Array<Object>;

  constructor(public navCtrl: NavController, public data: DataProvider, public _form: FormBuilder, public toastCtrl: ToastController, public navParams: NavParams, public http: Http, public loadingCtrl: LoadingController, public alertCtrl: AlertController) {

    this.signupForm = this._form.group({
      "surname": ["", Validators.compose([Validators.required])],

      "other_names": ["", Validators.compose([Validators.required])],
      "telco": ["", Validators.compose([Validators.required])],
      "mobile_number": ["", Validators.compose([Validators.required])],
      "email": ["", Validators.compose([Validators.required])],
      "user_type": ["", Validators.compose([Validators.required])],
      "pds": [""],
      "username": ["", Validators.compose([Validators.required])],
      "password": ["", Validators.compose([Validators.required])],
      "confirmPassword": ["", Validators.compose([Validators.required])],
      "dob": ["", Validators.compose([Validators.required])],
      "country_id": [""],
      "region_id": [""],
      "city_id": [""],
      "suburb_id": [""]


    }, {
        validator: PasswordValidation.MatchPassword // your validation method
      })

    this.date = new Date();
    this.maxSelectabledate = this.formatDatemax(this.date);

    if (this.signupForm.value.country_id) {
      console.log("this.signupForm.value.country_id = " + this.signupForm.value.country_id);
    }

  }

  ionViewWillEnter() {
    this.getcountries();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage');
  }


  signup() {

    this.signupVal = JSON.stringify(this.signupForm.value);

    this.jsonBody = JSON.parse(this.signupVal);

    console.log("THIS IS THE SIGNUP raw values VALUES" + this.signupVal)
    console.log("THIS IS THE SIGNUP VALUES" + this.jsonBody)

    // this.experimentmovetoAddregisPage();

    let loader = this.loadingCtrl.create({
      content: "Please wait ..."
    });

    loader.present();



    this.data.registration(this.jsonBody).then((result) => {

      console.log(result);
      var jsonBody = result["_body"];
      console.log(jsonBody);

      jsonBody = JSON.parse(jsonBody);
      console.log(jsonBody)

      var desc = jsonBody["resp_desc"];
      var code = jsonBody["resp_code"];
      var user_type1 = jsonBody["user_type1"];
      this.regid = jsonBody["regid"];


      console.log(desc);
      console.log(code);

      this.messageList = desc;
      this.api_code = code;

      loader.dismiss();

      if (this.api_code == "000") {
        let loader = this.loadingCtrl.create({
          content: "Signing up..."
          // duration: 5000


        });
        loader.present();


        if (user_type1 == "D") {
          setTimeout(() => {

            // let alert = this.alertCtrl.create({
            //   title: '',
            //   subTitle: "Sign up has been successful. Kindly complete the professional info form.",
            //   buttons: ['OK']
            // });
            // alert.present();


            let alert = this.alertCtrl.create({
              title: "",
              subTitle: "Sign up has been successful. Kindly complete the professional info form.",
              buttons: [
                { text: 'OK',
                    handler: () => {
                      // this.navCtrl.push(ProfessionalInfoPage);
                      this.navCtrl.setRoot(ProfessionalInfoPage, { regid: this.regid });
                    }
                }
              ]
            });
            alert.present();

            
          }, 3);


          setTimeout(() => {
            let alert = this.alertCtrl.create({
              title: '',
              subTitle: "Sign up has been successful. Kindly complete the professional info form.",
              buttons: ['OK']
            });
            loader.dismiss();
          }, 3);


          // this.toastCtrl.create({
          //   message: "Sign up has been successful. Kindly complete the professional info form.",
          //   duration: 5000
          // }).present();

        }
        else {

          setTimeout(() => {

            let alert = this.alertCtrl.create({
              title: '',
              subTitle: "Sign up has been successful. Kindly login..",
              buttons: [
                { text: 'OK',
                    handler: () => {
                      this.navCtrl.setRoot(LoginPage, { value: this.jsonBody });
                    }
                }
              ]
            });
            alert.present();
            

            
          }, 3);


          setTimeout(() => {
            let alert = this.alertCtrl.create({
              title: '',
              subTitle: "Sign up has been successful. Kindly login..",
              buttons: ['OK']
            });
            loader.dismiss();
          }, 3);


          // this.toastCtrl.create({
          //   message: "Sign up has been successful. Kindly login..",
          //   duration: 5000
          // }).present();

        }


      }

      else {
        let alert = this.alertCtrl.create({
          title: "",
          subTitle: this.messageList,
          buttons: ['OK']
        });
        alert.present();
      }
    }, (err) => {

      let alert = this.alertCtrl.create({
        title: "",
        subTitle: "Sign Up Unsuccessful",
        buttons: ['OK']
      });
      alert.present();

      // this.toastCtrl.create({
      //   message: "Please ensure all details provided are correct.",
      //   duration: 5000
      // }).present();
      loader.dismiss();
      console.log(err);
    });
  }



  getcountries() {

    this.data.get_countries().then((result) => {

      console.log("RESULTS IS " + result);
      var body = result["_body"];
      console.log("result body = " + JSON.stringify(result["_body"]));
      body = JSON.parse(body);
      this.countries = body;
      // console.log("RESULTS IS " + this.countries);
      // this.body = Array.of(this.countries);

    }, (err) => {
      console.log(err);
    });
  }

  signin() {

    this.navCtrl.push(LoginPage)
  }


  get_region(country_id) {

    if (country_id) {

      console.log("country_id = " + JSON.stringify(country_id));

      let loading = this.loadingCtrl.create({
        content: 'Please wait...'
      });

      loading.present();

      setTimeout(() => {
        this.data.get_regions_by_country(country_id)
          .then(result => {

            console.log(result);
            var jsonBody = result["_body"];
            jsonBody = JSON.parse(jsonBody);
            this.regions = jsonBody;
            loading.dismiss();

            console.log("Jsson body " + JSON.stringify(jsonBody));
          }, (err) => {

            loading.dismiss();
            this.showalertmessage("Ghinger", "Sorry. An Error occured. Kindly refresh and try again.");
            this.showmessage("Sorry. An Error occured. Kindly refresh and try again.");
            console.log("error = " + JSON.stringify(err));
          });

      }, 1);
    }

  }

  get_cities(region_id) {

    if (region_id) {

      console.log("region_id = " + JSON.stringify(region_id));

      let loading = this.loadingCtrl.create({
        content: 'Please wait...'
      });

      loading.present();

      setTimeout(() => {
        this.data.get_cities_by_region(region_id)
          .then(result => {

            console.log(result);
            var jsonBody = result["_body"];
            jsonBody = JSON.parse(jsonBody);
            this.cities = jsonBody;
            loading.dismiss();

            console.log("Jsson body " + JSON.stringify(jsonBody));
          }, (err) => {

            loading.dismiss();
            this.showalertmessage("Ghinger", "Sorry. An Error occured. Kindly refresh and try again.");
            this.showmessage("Sorry. An Error occured. Kindly refresh and try again.");
            console.log("error = " + JSON.stringify(err));
          });

      }, 1);
    }
  }


  get_suburbs(city_id) {

    if (city_id) {

      console.log("city_id = " + JSON.stringify(city_id));

      let loading = this.loadingCtrl.create({
        content: 'Please wait...'
      });

      loading.present();

      setTimeout(() => {
        this.data.get_suburbs_by_city(city_id)
          .then(result => {

            console.log(result);
            var jsonBody = result["_body"];
            jsonBody = JSON.parse(jsonBody);
            this.suburbs = jsonBody;
            loading.dismiss();

            console.log("Jsson body " + JSON.stringify(jsonBody));
          }, (err) => {

            loading.dismiss();
            this.showalertmessage("Ghinger", "Sorry. An Error occured. Kindly refresh and try again.");
            this.showmessage("Sorry. An Error occured. Kindly refresh and try again.");
            console.log("error = " + JSON.stringify(err));
          });

      }, 1);
    }
  }



  experimentmovetoAddregisPage() {
    let loader = this.loadingCtrl.create({
      content: "Please wait ..."
    });

    loader.present();
    setTimeout(() => {

      let alert = this.alertCtrl.create({
        title: '',
        subTitle: "Sign up has been successful. Kindly complete the professional info form.",
        // subTitle: "Sign up has been successful. A customer service person will contact you shortly to process your registration.",
        buttons: ['OK']
      });
      alert.present();

      // this.navCtrl.setRoot(LoginPage, { value: this.jsonBody });
      this.navCtrl.push(ProfessionalInfoPage);

    }, 3);


    setTimeout(() => {
      let alert = this.alertCtrl.create({
        title: '',
        subTitle: "Sign up has been successful. Kindly complete the professional info form.",
        buttons: ['OK']
      });
      loader.dismiss();
    }, 3);


    this.toastCtrl.create({
      message: "Sign up has been successful. Kindly complete the professional info form.",
      duration: 5000
    }).present();
  }

  formatDate(date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + (d.getDate()),
      year = d.getFullYear();

    // console.log("year" + year + "and day = " + day);


    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    console.log("year" + year + "and day = " + day);

    return [year, month, day].join('-');
  }

  formatDatemax(date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + (d.getDate()),
      year = d.getFullYear();

    console.log("year" + year + "and day = " + day);


    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    console.log("year" + year + "and day = " + day);

    return [year, month, day].join('-');
  }


  showmessage(message) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }

  showalertmessage(titlemsg, mainmsg) {
    let alert = this.alertCtrl.create({
      title: titlemsg,
      subTitle: mainmsg,
      buttons: ['OK']
    });
    alert.present();
  }


}
