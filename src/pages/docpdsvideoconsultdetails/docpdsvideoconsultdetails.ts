import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { ToastController, LoadingController, AlertController, ModalController } from 'ionic-angular';
import { DataProvider } from '../../providers/data/data';
import { CompleteTestService } from '../../providers/complete-test-service/complete-test-service';
// // import { Keyboard } from '@ionic-native/keyboard';
import 'rxjs/add/operator/map';

@Component({
  selector: 'page-docpdsvideoconsultdetails',
  templateUrl: 'docpdsvideoconsultdetails.html',
})
export class DocpdsvideoconsultDetailsPage {
  @ViewChild('searchbar') myInput;
  @ViewChild('input')
    // searchbar: any;

  messageList: any;
  api_code: any;
  location: any;
  displayData: any;
  check: any;
  from_menu: any = [];
  body: any;
  jsonBody: any;
  params: any = [];
  newparams: any;
  from_login: any = [];
  from_login2: any = [];
  from_login3: any = [];
  sub_id: any;
  string: any;

  requester_id1: any;
  from_login_doc: any = [];
  from_login_pers: any = [];
  body1: any;
  retrieve1: any;
  jsonBody1: any;
  

  body2: any;
  jsonBod2: any;
  pds_appointments: any = [];
  content: any = [];
  rowid: any;
  appointmentType: any;
  doc_pds_videoconsult : any;
  doc_pds_videoconsults : any;

  constructor(
    // private keyboard: Keyboard, 
    public navCtrl: NavController, public navParams: NavParams, public completeTestService: CompleteTestService, public data: DataProvider, public loadingCtrl: LoadingController, public alertCtrl: AlertController, public modalCtrl: ModalController, public viewCtrl: ViewController,public toastCtrl: ToastController) {

    // console.log("We are in Home Care Consult Appointments History page");
    // this.from_login = this.navParams.get('value');

    // this.from_login2 = this.navParams.get('pers_value');
    // this.from_login3 = this.navParams.get('doc_value');
    this.doc_pds_videoconsult = this.navParams.get("doc_pds_videoconsult");
    if(this.doc_pds_videoconsult){
      this.doc_pds_videoconsults = Array.of(this.doc_pds_videoconsult);
      // this.getdoc_pdsvideoconsult_details(this.doc_pds_videoconsult_id);
    }
    // console.log('VALUE IN TABS CONSTRUCTOR IS' + this.from_login);

    // if(this.from_login){
    //   this.body = Array.of(this.from_login);
    //   this.jsonBody = JSON.parse(this.body);

    //   if(this.jsonBody[0]){
    //     if(this.jsonBody[0].id){
    //       this.requester_id1 = this.jsonBody[0].id;
    //     }    
    //   }
    // }

    
    // // this.check = this.jsonBody[0];

    // if(this.requester_id1){
    //   if(this.appointmentType){
    //     this.params = {
    //       "requester_id": this.requester_id1,
    //       "appointment_type_id": this.appointmentType
    //     }

    //     if(this.params){
    //       this.newparams = JSON.stringify(this.params);
    //       console.log("New params " + this.newparams);
    //     }
    //   }
    // }

    
 
  }

  closeModal() {
    this.viewCtrl.dismiss();
  }

  attend_to(appt_id){
    //make request to accept pds appt with id= appt_id
    this.showalertmessage("Ghinger","This action cannot be taken at the moment.");

    //the desired action: show this prompt: You are about to accept the selected appointment, do you want to continue?
  }

  

  getdoc_pdsvideoconsult_details(appt_id) {

    if(appt_id){
      
      let loading = this.loadingCtrl.create({
        content: 'Please wait...'
      });
  
      loading.present();
  
      setTimeout(() => {        
        this.data.get_doc_pds_appointments_videoconsult_details(appt_id)
          .then(result => {
            
            console.log(result);
            var jsonBody = result["_body"];
            jsonBody = JSON.parse(jsonBody);
            this.doc_pds_videoconsults = jsonBody;
            loading.dismiss();

            console.log("Jsson body " +JSON.stringify(jsonBody));
          }, (err) => {

            loading.dismiss();
            this.showalertmessage("Ghinger", "Sorry. An Error occured. Kindly refresh and try again.");
            this.showmessage("Sorry. An Error occured. Kindly refresh and try again.");
            console.log("error = "+JSON.stringify(err));
          });
    
        }, 1);
    } 

  }

  showmessage(message){
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }

  showalertmessage(titlemsg, mainmsg){
    let alert = this.alertCtrl.create({
      title: titlemsg,
      subTitle: mainmsg,
      buttons: ['OK']
    });
    alert.present();
  }


}
