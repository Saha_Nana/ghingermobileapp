import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { ToastController, LoadingController, AlertController, ModalController } from 'ionic-angular';
import { DataProvider } from '../../providers/data/data';
import { MedappointmentdetailsPage } from '../medappointmentdetails/medappointmentdetails';
import { SearchPage } from '../search/search';
import { CompleteTestService } from '../../providers/complete-test-service/complete-test-service';
// import { Keyboard } from '@ionic-native/keyboard';
import 'rxjs/add/operator/map';


@Component({
  selector: 'page-search2',
  templateUrl: 'search2.html',
})
export class Search2Page {
  @ViewChild('searchbar') myInput;
  @ViewChild('input')
  searchbar: any;

  messageList: any;
  api_code: any;
  location: any;
  displayData: any;
  check: any;
  from_menu: any = [];
  body: any;
  jsonBody: any;
  params: any = [];
  newparams: any;
  from_login: any = [];
  from_login2: any = [];
  from_login3: any = [];
  sub_id: any;
  string: any;

  requester_id1: any;
  from_login_doc: any = [];
  from_login_pers: any = [];
  body1: any;
  retrieve1: any;
  jsonBody1: any;

  body2: any;
  jsonBod2: any;

  // person_type3: any;

  person_details: any = [];
  content: any = [];
  // items: any = [];
  medappointmentdetails = { id: '' };
  rowid: any;

  constructor(
    // private keyboard: Keyboard, 
    public navCtrl: NavController, public navParams: NavParams, public completeTestService: CompleteTestService, public data: DataProvider, public loadingCtrl: LoadingController, public alertCtrl: AlertController, public modalCtrl: ModalController, public viewCtrl: ViewController) {
    console.log("We are in Medical Appointments History page");
    this.from_login = this.navParams.get('value');

    this.from_login2 = this.navParams.get('pers_value');
    this.from_login3 = this.navParams.get('doc_value');
    console.log('VALUE IN TABS CONSTRUCTOR IS' + this.from_login);

    this.body = this.from_login; // this.body = Array.of(this.from_login);

    this.jsonBody = this.body; // this.jsonBody = JSON.parse(this.body);

    this.requester_id1 = this.jsonBody[0].id;
    this.check = this.jsonBody[0];

    console.log('VALUE IN medical history IS' + this.from_login);
    console.log('VALUE of requester IN medical appointment history  IS ' + this.requester_id1);

    this.params = {
      "requester_id": this.requester_id1
    }

    this.newparams = this.params; // this.newparams = JSON.stringify(this.params);

    console.log("New params " + this.newparams);


    // this.content = {
    //   title: 'Title text',
    //   text: 'Text content'
    // }
    // console.log(tcontent);

    // this.person_details.push(this.params);

    // console.log(this.params);

    // this.jsonBod2 = JSON.parse(this.params);

    // this.body2 = Array.of(this.params);

    // this.jsonBod2 = JSON.parse(this.body2);

    // // this.requester_id1 = this.jsonBody[0].id


    // console.log("REQUESTER ID: " + this.params.requester_id);

    this.getAppointmentHistory();
  }



  closeModal() {


    this.viewCtrl.dismiss();
  }



  openNewAppointment(){

    //suppose to open the new medical appointment modal page
    this.navCtrl.push(SearchPage, {
      value: this.from_login, doc_value: this.from_login3, pers_value: this.from_login2
     });
  }
  // showvalues(){
  //   console.log("this.from_login = "+ this.from_login);
  //   console.log("this.from_login_doc = "+ this.from_login_doc);
  //   console.log("this.from_login_pers = "+ this.from_login_pers);
  // }

  getAppointmentHistory() {

    this.jsonBody = this.newparams; // this.jsonBody = JSON.parse(this.newparams);

    this.data.med_appointment_history(this.jsonBody)
      .then(result => {
        // this.contacts = result;
        console.log(result);

        var jsonBody = result["_body"];
        jsonBody = JSON.parse(jsonBody);
        this.person_details = jsonBody;

        console.log("Jsson body " +jsonBody);
      });

  }

  // medical_appointment_history_details(person_details) {
  //   // if (this.requester_id == null) {

  //   //   let alert = this.alertCtrl.create({
  //   //     title: '',
  //   //     subTitle: NOT_PERMITTED,
  //   //     buttons: ['OK']
  //   //   });
  //   //   alert.present();
  //   // }

  //   // if (this.doc_id == null && this.person_type_id == "D") {

  //   //   let alert = this.alertCtrl.create({
  //   //     title: '',
  //   //     subTitle: NOT_PERMITTED2,
  //   //     buttons: ['OK']
  //   //   });
  //   //   alert.present();
  //   // }

  //   // if (this.requester_id != null && this.doc_id == null && this.person_type_id == "C") {

  //     console.log('pushing to med appointment history details');

  //     // this.navCtrl.push(LocationPage, { value: this.from_login, doc_value: this.from_login_doc, pers_value: this.from_login_pers });
  //   // this.navCtrl.push(MedappointmentdetailsPage, { value: this.from_login, doc_value: this.from_login_doc, pers_value: this.from_login_pers });
  //     // let modal = this.modalCtrl.create(MedappointmentdetailsPage, {

  //     //   // value: this.from_login, pers_value: this.from_login_pers, doc_value: this.from_login_doc

  //     // });

  //     // modal.present();
  //   this.navCtrl.push(MedappointmentdetailsPage, {
  //     value: this.from_login, doc_value: this.from_login_doc, pers_value: this.from_login_pers
  //   });
  //   // }
  // }


  medical_appointment_history_details(medappointhistory) {
    // console.log("in search 2 page: rowid = "+ medappointhistory_id);

    this.navCtrl.push(MedappointmentdetailsPage, { value: this.from_login, doc_value: this.from_login_doc, pers_value: this.from_login_pers,medappointhistory: medappointhistory  });
    // rowid: rowid
  }



  // go() {

  //   let loader = this.loadingCtrl.create({
  //     content: "Please wait ..."

  //   });

  //   loader.present();

  //   let data = this.searchbar.getValue()

  //   console.log("LOCATION ENTERED " + data)

  //   if (data == undefined) {

  //     let alert = this.alertCtrl.create({
  //       title: "",
  //       subTitle: "Please enter your location.Your location should be on ghinger.",
  //       buttons: ['OK']
  //     });
  //     alert.present();
  //     loader.dismiss();
  //   }




  //   // if (data != ""){
  //   else {
  //     this.params = {

  //       "location": data

  //     }
  //     console.log('PARAMETERS' + this.params);

  //     this.data.hospitals(this.params).then((result) => {

  //       console.log("RESULTS IS " + result);
  //       console.log("RESULTS IS" + this.data.hospitals(this.params));
  //       var body = result["_body"];
  //       body = JSON.parse(body);
  //       this.check = body
  //       console.log("RESULTS IS " + this.check);
  //       this.string = JSON.stringify(this.check)
  //       console.log("LETS SEE THE STRING " + this.string)

  //       this.jsonBody = JSON.parse(this.string);

  //       this.sub_id = this.jsonBody[0].suburb_id
  //       console.log("LETS SEE THE Surburb " + this.sub_id)



  //       var desc = body["resp_desc"];
  //       var code = body["resp_code"];


  //       console.log(desc);
  //       console.log(code);

  //       this.messageList = desc;
  //       this.api_code = code;

  //        this.viewCtrl.dismiss();

  //     }, (err) => {


  //       console.log(err);
  //     });



  //     console.log("VALUES FROM LOCATION SEARCH" + data);
  //     console.log(data);
  //     console.log("VALUES FROM LOCATION IN ID " + this.sub_id);

  //     setTimeout(() => {

  //       this.navCtrl.push(HospitalListPage, { value: data, another: this.from_login, sub_id: this.sub_id, doc_value: this.from_login3, pers_value: this.from_login2 }, );

  //   }, 3000);

  //     setTimeout(() => {
  //       loader.dismiss();
  //     }, 3000);

  //   }
  // }


}
