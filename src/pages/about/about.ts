import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { LoadingController,AlertController   } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { Storage } from '@ionic/storage';
import { DoctorHomePage } from '../doctorhome/doctorhome';
import { MenuPage } from '../menu/menu';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {

  user_login_type : any;

  constructor(public navCtrl: NavController,public loadingCtrl:LoadingController,public alertCtrl: AlertController,public storage: Storage) {
    
    this.storage.get('user_login_type').then((user_login_type) => {
      this.user_login_type = user_login_type;
      console.log(' About user_login_type = ' + user_login_type);
    });

    //  showConfirm() {
    let confirm = this.alertCtrl.create({
      title: 'Logout?',
      message: 'Do you agree to logout ?',
      buttons: [
        {
          text: 'No',
          handler: () => {
            // console.log('Disagree clicked');
            // this.storage.set("user_login_type Doctor Patient

            // this.navCtrl.pop();

            // if(this.user_login_type){
            //   if(this.user_login_type == "Doctor"){
            //     this.navCtrl.push(DoctorHomePage);
            //   }
            //   else if(this.user_login_type == "Patient"){
            //     this.navCtrl.push(MenuPage);
            //   }
            
            // }

            
          }
        },
        {
          text: 'Yes',
          handler: () => {
             let loader = this.loadingCtrl.create({
              content: "Logging out...",
              duration: 5000
            });

            loader.present();
     
            setTimeout(() => {
              console.log("logging out in 1 second");
              this.storage.clear();
              this.storage.set('remember', false);
              this.navCtrl.setRoot(LoginPage);
        
            }, 3);

            setTimeout(() => {
              loader.dismiss();
            }, 3);

          }
        }
      ]
    });
    confirm.present();
  }

  }

// }
