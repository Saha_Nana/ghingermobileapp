import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { ToastController, LoadingController, AlertController, ModalController } from 'ionic-angular';
import { DataProvider } from '../../providers/data/data';
// import { Search2Page } from '../search2/search2';
// import { SearchPage } from '../search/search';
import { CompleteTestService } from '../../providers/complete-test-service/complete-test-service';
// import { Http } from '@angular/http';
// import { Keyboard } from '@ionic-native/keyboard';
import 'rxjs/add/operator/map';


/**
 * Generated class for the MedappointmentdetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

// @IonicPage()
@Component({
  selector: 'page-medappointmentdetails',
  templateUrl: 'medappointmentdetails.html',
})
export class MedappointmentdetailsPage {

  currentmedappointmentdetail: any;
  currentmedappointmentdetaildata = { id: 0, location: '', serviceprovider: '', requestcategory: '', beneficiary: '', requesturgency: '', proposeddateandtime: '', complaint: '', prevmedicalhistory: '', allergies: '', source: '', confirmstatus: '', beneficiary_phone_number: '', beneficiary_age: '', beneficiary_gender: '',confirmed_date : '' };
  params: any = [];
  newparams: any;
  appointment_id: any;
  jsonBody: any;
  body: any;
  retrieve1: string;
  body1: any;
  jsonBody1: any;
  appointment : any;

  constructor(
    // private keyboard: Keyboard, 
    public navCtrl: NavController, public navParams: NavParams, public completeTestService: CompleteTestService, public data: DataProvider, public loadingCtrl: LoadingController, public alertCtrl: AlertController, public modalCtrl: ModalController, public viewCtrl: ViewController) {

    this.appointment = navParams.get("medappointhistory")

    // this.params = {
    //   "appointment_id": this.appointment_id
    // }

    // this.newparams = JSON.stringify(this.params);

    // console.log("appointment_id = " + this.newparams);
    this.getCurrentMedAppointmentdetails(this.appointment);
    this.read_appointment(this.appointment.id);
    // appointment_id
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MedappointmentdetailsPage');
  }

  getCurrentMedAppointmentdetails(medappointhistory) {

    console.log("medappointhistory = " + JSON.stringify(medappointhistory));


    // var body1 = JSON.parse(medappointhistory);

    // this.retrieve1 = JSON.stringify(body1)

    // console.log('LETS SEE THE  BODY ' + body1);
    // console.log('LETS SEE THE DATA RETRIEVED ' + this.retrieve1);

    this.jsonBody1 = Array.of(medappointhistory)
    // this.jsonBody1 = JSON.parse(this.body1);
    // console.log("medappointdetails page line 92 : this.jsonBody1[0].id = " + this.jsonBody1[0].id);

    //****************

    // this.jsonBody1 = medappointhistory;

    if (this.jsonBody1) {
      if (this.jsonBody1[0]) {
        //   this.currentmedappointmentdetaildata.id = data["id"];
        this.currentmedappointmentdetaildata.location = this.jsonBody1[0].suburb_name;
        this.currentmedappointmentdetaildata.serviceprovider = this.jsonBody1[0].provider_name;
        this.currentmedappointmentdetaildata.requestcategory = this.jsonBody1[0].category;
        this.currentmedappointmentdetaildata.beneficiary = this.jsonBody1[0].beneficiary_name;
        this.currentmedappointmentdetaildata.beneficiary_phone_number = this.jsonBody1[0].beneficiary_phone_number;
        this.currentmedappointmentdetaildata.beneficiary_age = this.jsonBody1[0].beneficiary_age;

        if (this.jsonBody1[0].beneficiary_gender) {
          if (this.jsonBody1[0].beneficiary_gender == "F") {
            this.currentmedappointmentdetaildata.beneficiary_gender = "Female";
          } else if (this.jsonBody1[0].beneficiary_gender == "M") {
            this.currentmedappointmentdetaildata.beneficiary_gender = "Male";
          } else {
            this.currentmedappointmentdetaildata.beneficiary_gender = "";
          }
        }
        this.currentmedappointmentdetaildata.requesturgency = this.jsonBody1[0].urgency;
        this.currentmedappointmentdetaildata.proposeddateandtime = this.jsonBody1[0].proposed_date;
        this.currentmedappointmentdetaildata.confirmed_date = this.jsonBody1[0].confirmed_date;
        this.currentmedappointmentdetaildata.complaint = this.jsonBody1[0].complaint_desc;
        this.currentmedappointmentdetaildata.prevmedicalhistory = this.jsonBody1[0].prev_medical_history;
        this.currentmedappointmentdetaildata.allergies = this.jsonBody1[0].allergies;

        if (this.jsonBody1[0].src == 'APP') {
          this.currentmedappointmentdetaildata.source = "Mobile App";
        }

        if (this.jsonBody1[0].confirm_status == true) {
          this.currentmedappointmentdetaildata.confirmstatus = "Confirmed";
        }
        else {
          this.currentmedappointmentdetaildata.confirmstatus = "Not Confirmed";
        }

      }
    }





    // this.jsonBody = JSON.parse(this.newparams);

    // console.log("this.newparams = " + this.newparams + "jsonBody = " + this.jsonBody);

    // this.data.getCurrentMedAppointmentdetails(this.jsonBody)
    //   .then(data => {

    //     // ***********

    //     var body1 = data["_body"];
    //     body1 = JSON.parse(body1);

    //     this.retrieve1 = JSON.stringify(body1)

    //     // console.log('LETS SEE THE  BODY ' + body1);
    //     console.log('LETS SEE THE DATA RETRIEVED ' + this.retrieve1);

    //     this.body1 = Array.of(this.retrieve1)
    //     this.jsonBody1 = JSON.parse(this.body1);
    //     // console.log("medappointdetails page line 92 : this.jsonBody1[0].id = " + this.jsonBody1[0].id);

    //     //****************

    //     if (this.jsonBody1) {
    //       if (this.jsonBody1[0]) {
    //         //   this.currentmedappointmentdetaildata.id = data["id"];
    //         this.currentmedappointmentdetaildata.location = this.jsonBody1[0].suburb_name;
    //         this.currentmedappointmentdetaildata.serviceprovider = this.jsonBody1[0].provider_name;
    //         this.currentmedappointmentdetaildata.requestcategory = this.jsonBody1[0].category;
    //         this.currentmedappointmentdetaildata.beneficiary = this.jsonBody1[0].beneficiary_name;
    //         this.currentmedappointmentdetaildata.beneficiary_phone_number = this.jsonBody1[0].beneficiary_phone_number;
    //         this.currentmedappointmentdetaildata.beneficiary_age = this.jsonBody1[0].beneficiary_age;

    //         if (this.jsonBody1[0].beneficiary_gender) {
    //           if (this.jsonBody1[0].beneficiary_gender == "F") {
    //             this.currentmedappointmentdetaildata.beneficiary_gender = "Female";
    //           } else if (this.jsonBody1[0].beneficiary_gender == "M") {
    //             this.currentmedappointmentdetaildata.beneficiary_gender = "Male";
    //           } else {
    //             this.currentmedappointmentdetaildata.beneficiary_gender = "";
    //           }
    //         }
    //         this.currentmedappointmentdetaildata.requesturgency = this.jsonBody1[0].urgency;
    //         this.currentmedappointmentdetaildata.proposeddateandtime = this.jsonBody1[0].proposed_date;
    //         this.currentmedappointmentdetaildata.complaint = this.jsonBody1[0].complaint_desc;
    //         this.currentmedappointmentdetaildata.prevmedicalhistory = this.jsonBody1[0].prev_medical_history;
    //         this.currentmedappointmentdetaildata.allergies = this.jsonBody1[0].allergies;

    //         if (this.jsonBody1[0].src == 'APP') {
    //           this.currentmedappointmentdetaildata.source = "Mobile App";
    //         }

    //         if (this.jsonBody1[0].confirm_status == true) {
    //           this.currentmedappointmentdetaildata.confirmstatus = "Confirmed";
    //         }
    //         else {
    //           this.currentmedappointmentdetaildata.confirmstatus = "Not Confirmed";
    //         }

    //       }
    //     }


    //     // console.log(this.currentmedappointmentdetaildata);
    //   });
  }

  read_appointment(data) {

    this.params = {"appointment_id": data}
    
    this.data.read_appointment(this.params)
      .then(result => {});
  
  }

  closeModal() {
    this.viewCtrl.dismiss();
  }


}
