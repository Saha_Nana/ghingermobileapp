import { Component, ViewChild } from '@angular/core';
import { MenuController, NavController, NavParams, App } from 'ionic-angular';
import { CompleteTestService } from '../../providers/complete-test-service/complete-test-service';
import { MedAppointPage } from '../med-appoint/med-appoint';
import { PersonalWelPage } from '../personal-wel/personal-wel';
import { VidConsultPage } from '../vid-consult/vid-consult';
import { LabHistoryPage } from '../lab-history/lab-history';
import { MedicationSearchPage } from '../medication-search/medication-search';
import { HomeSearchPage } from '../home-search/home-search';
import { DocHistoryPage } from '../doc-history/doc-history';
import { LocationPage } from '../location/location';
import { LoginPage } from '../login/login';
import { SearchPage } from '../search/search';
import { Search2Page } from '../search2/search2';
import { MedicationhistoryPage } from "../medicationhistory/medicationhistory";
import { LabSearchPage } from '../lab-search/lab-search';
import { DataProvider } from '../../providers/data/data';
import { ToastController, LoadingController, AlertController, ModalController } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { MedappointmentdetailsPage } from "../medappointmentdetails/medappointmentdetails";
import { Labhistory1Page } from "../labhistory1/labhistory1";
import { VideoconsulthistoryPage } from "../videoconsulthistory/videoconsulthistory";
import { HomecarehistoryPage } from "../homecarehistory/homecarehistory";
import { Storage } from '@ionic/storage';


const NOT_PERMITTED = "Please your account has not been verified. Kindly be patient. An agent would speak to you soon to get your account active!"
const NOT_PERMITTED2 = "You are not allowed to order for personal doctor service."



@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',

})
export class MenuPage {

  @ViewChild('searchbar')

  searchbar: any;
  from_login: any = [];
  from_login_doc: any = [];
  from_login_pers: any = [];
  messageList: any;
  person_type_id: any;
  api_code: any;
  location: any;
  displayData: any;
  check: any;
  from_menu: any = [];
  body: any;
  jsonBody: any;
  params: any = [];
  parsed_name: any;
  requester_id: any;
  patient_id: any;
  doc_id: any;
  doc_id1: any;
  doc_id2: any;
  data1: any = [];
  username: any;
  retrieve: string;
  retrieve1: string;
  retrieve_pers: string;
  retrieve_doc: string;
  retrieve_doc3: string;
  body1: any;
  jsonBody1: any;
  person_type: any;
  person_type1: any;
  doctor_id: any;
  id: any;
  doctor_id2: any;
  doc_params: any = [];
  doc_params2: any = [];

  body2: any;
  retrieve2: any;
  jsonBody2: any;
  person_type2: any;
  doctor_id3: any;
  body3: any;
  retrieve3: any;
  jsonBody3: any;
  person_type3: any;
  body4: any;
  retrieve4: any;
  jsonBody4: any;
  body5: any;
  retrieve5: any;
  jsonBody5: any;

  myparams: any;
  newparams: any;
  appointment_statistics1: any = [];
  myjsonBody: any;
  string: any;
  from_regis_id: any = [];
  surname: string = "";
  other_names: string = "";
  full_name: string = "";

  medical_appointment_count: any;
  medication_appointment_count: any;

  labrequest_appointment_count: any;
  total_patient_pds_appointment_count: any;
  videoconsult_appointment_count: any;
  homecare_appointment_count: any;

  constructor(public app: App, public menuCtrl: MenuController, public data: DataProvider, public alertCtrl: AlertController, public modalCtrl: ModalController, public navCtrl: NavController, public navParams: NavParams, public completeTestService: CompleteTestService, public loadingCtrl: LoadingController, public storage: Storage) {



    this.menuCtrl.enable(true);
    // this.from_login = this.navParams.get('value')
    // this.from_login_doc = this.navParams.get('doc_value')
    // this.from_login_pers = this.navParams.get('pers_value');


    this.storage.get('value').then((value) => {
      this.from_login = value

      console.log('LOGIN DETAILS IN MENU PAGE CONSTRUCTOR IS ' + JSON.stringify(value));
      // console.log('LOGIN DETAILS IN MENU PAGE CONSTRUCTOR IS ' + JSON.stringify(this.from_login));

      this.body2 = this.from_login; // this.body2 = JSON.parse(this.from_login);
      // this.retrieve2 = this.body2
      this.retrieve2 = this.body2; // this.retrieve2 = JSON.stringify(this.body2);

      console.log('PADMORE, LETS SEE THE  BODY ' + this.body2);
      console.log('PADMORE, LETS SEE THE DATA RETRIEVED ' + this.retrieve2);
      // this.body2 = Array.of(this.retrieve2)
      this.jsonBody2 = this.retrieve2;  // this.jsonBody2 = JSON.parse(this.body2);
      this.person_type2 = this.jsonBody2[0].user_type
      this.doctor_id3 = this.jsonBody2[0].id

      console.log('PADMORE, THIS JSON BODY IN MENU PAGE CONSTRUCTOR IS' + this.jsonBody2);
      console.log('PADMORE, LETS SEE person_type2 RETRIEVED ' + this.person_type2);
      console.log('PADMORE, LETS SEE doctor_id3 RETRIEVED ' + this.doctor_id3);

      this.body3 = this.from_login; // this.body3 = JSON.parse(this.from_login);
      this.retrieve3 = this.body3;  // this.retrieve3 = JSON.stringify(this.body3);
      this.body3 = this.retrieve3; // this.body3 = Array.of(this.retrieve3);

      console.log('THIS.BODY IN MENU PAGE CONSTRUCTOR IS' + this.body3);

      // console.log("output this body without passing json " + this.body[0].id);
      this.jsonBody3 = this.body3; // this.jsonBody3 = JSON.parse(this.body3);
      // console.log('THIS JSON BODY IN MENU PAGE CONSTRUCTOR IS' + this.jsonBody3);
      // this.requester_id = this.jsonBody3[0].id
      // console.log('VALUE of requester IN Menu  IS ' + this.requester_id);
      // this.storage.set
      // this.username = this.jsonBody3[0].username
      // this.check = this.jsonBody[0]
      // console.log('Padmore,Menu.ts line 157');
      // console.log("USERNAME HERE IS " + this.username);


      if (this.from_login) {

        console.log('LOGIN DETAILS IN DOC PAGE CONSTRUCTOR IS' + this.from_login);
        // this.body = Array.of(this.from_login)
        this.jsonBody = this.from_login; // this.jsonBody = JSON.parse(this.body);

        if (this.jsonBody) {
          if (this.jsonBody[0]) {
            this.username = this.jsonBody[0].username;

            if (this.jsonBody[0].id) {
              this.requester_id = this.jsonBody[0].id;
              this.storage.set('requester_id', this.requester_id);
              console.log("requester ID = " + this.requester_id);

              this.getappointments_counts(this.requester_id);
            }
            // this.doctor_id = this.jsonBody[0].id;
            console.log("PATIENT's Home page this.jsonBody[0].id (doctor_id) = " + this.jsonBody[0].id);
            // this.getappointments_counts(this.jsonBody[0].id);
            // this.storage.set("doctor_id", this.jsonBody[0].id);
            // this.reg_id = this.jsonBody[0].reg_id;

            if (this.jsonBody[0].surname) {
              this.surname = this.jsonBody[0].surname;
            }
            if (this.jsonBody[0].other_names) {
              this.other_names = this.jsonBody[0].other_names
            }

            if (this.surname && this.other_names) {
              this.storage.set("doctor_name", this.surname + " " + this.other_names);
            }


          }
        }
      }

    });

    this.storage.get('doc_value').then((doc_value) => {
      this.from_login_doc = doc_value

      console.log('LOGIN DETAILS from LOGIN DOC IN MENU PAGE FOR CONSTRUCTOR IS' + this.from_login_doc);

    });
    this.storage.get('pers_value').then((pers_value) => {
      this.from_login_pers = pers_value

      console.log('LOGIN PERS VALUE IN MENU PAGE CONSTRUCTOR IS' + this.from_login_pers);
      console.log("this.from_login_pers = " + this.from_login_pers);
      this.body4 = this.from_login_pers; // this.body4 = JSON.parse(this.from_login_pers);
      this.retrieve4 = this.body4; // this.retrieve4 = JSON.stringify(this.body4);
      console.log('Padmore,Menu.ts line 153');
      // this.body4 = Array.of(this.retrieve4)
      this.jsonBody4 = this.retrieve4; // this.jsonBody4 = JSON.parse(this.body4);
      console.log('Padmore,Menu.ts line 156');
      this.doc_id = this.jsonBody4[0].doctor_id
      this.doc_id2 = this.jsonBody4[0].id
      console.log('Padmore,Menu.ts line 159');
      this.person_type_id = this.jsonBody4[0].person_type_id
      // this.check = this.jsonBody[0]
      console.log('Padmore,Menu.ts line 162 - person_type_id = ' + this.person_type_id);

      console.log('VALUE of DOCTOR ID IN Menu  IS ' + this.doc_id);
      console.log('VALUE of DOCTOR ID FROM DOC MODULE IN Menu  IS ' + this.doc_id2);
      console.log('VALUE of PERSON TYPE VALUE IN Menu  IS ' + this.person_type_id);

    });


    // this.from_regis_id = this.navParams.get('results_1');


    console.log('-------------------------------BEGIN TEST-----------------------------------------------');

    //another badge


    // console.log('VALUE of patient_id IN Menu  IS ' + this.patient_id);

    this.myparams = {
      "requester_id": this.requester_id
    }

    this.newparams = JSON.stringify(this.myparams);
    console.log("line 185 - menu = newparams " + this.newparams);
    // this.appointment_statistics();





    console.log('-------------------------------END TEST-----------------------------------------------');

    // console.log("Hello Padmore, In menu.ts line 83");
    //   this.body = Array.of(this.from_login)
    //   console.log('THIS.BODY IN MENU PAGE CONSTRUCTOR IS' + this.body);

    //   // console.log("output this body without passing json " + this.body[0].id);
    //   this.jsonBody = JSON.parse(this.body);
    //   console.log('THIS JSON BODY IN MENU PAGE CONSTRUCTOR IS' + this.jsonBody);
    //   this.requester_id = this.jsonBody[0].id
    //   this.username = this.jsonBody[0].username
    //   // this.check = this.jsonBody[0]

    //   this.body = Array.of(this.from_login_pers)
    //   this.jsonBody = JSON.parse(this.body);
    //   this.doc_id = this.jsonBody[0].doctor_id
    //   this.doc_id2 = this.jsonBody[0].id
    //   this.person_type_id = this.jsonBody[0].person_type_id
    //   // this.check = this.jsonBody[0]

    //    this.body = Array.of(this.from_login_doc)
    //   this.jsonBody = JSON.parse(this.body);
    //   this.patient_id = this.jsonBody[0].patient_id

    //   console.log("USERNAME HERE IS " + this.username)
    //   console.log('VALUE of requester IN Menu  IS ' + this.requester_id);
    //   console.log('VALUE of patient_id IN Menu  IS ' + this.patient_id);
    //   console.log('VALUE of DOCTOR ID IN Menu  IS ' + this.doc_id);
    //   console.log('VALUE of DOCTOR ID FROM DOC MODULE IN Menu  IS ' + this.doc_id2);
    //   console.log('VALUE of PERSON TYPE VALUE IN Menu  IS ' + this.person_type_id);



  }



  openMenu() {



    if (this.menuCtrl.isOpen()) {
      console.log("is open");
    }
    if (this.menuCtrl.isEnabled()) {
      console.log("is enabled");
    }

    this.menuCtrl.toggle();
    this.menuCtrl.open();

  }


  // ionViewDidLeave(){
  //     this.navCtrl.pop();
  // }


  ionViewDidLoad() {
    this.menuCtrl.enable(true);
  }

  ionViewDidEnter() {
    this.menuCtrl.enable(true);


  }

  ionViewWillEnter() {
    this.menuCtrl.enable(true);

    // this.events.subscribe('doc_new_appoint_counter:refreshpage', () => {

    if (this.requester_id) {
      this.getappointments_counts(this.requester_id);
    } else {
      this.storage.get('requester_id').then((requester_id) => {
        if (requester_id) {
          this.requester_id = requester_id;
          console.log(' Docgeneralappointmentlists page requester_id = ' + requester_id);
          this.getappointments_counts(this.requester_id);
        }
      });
    }

    // });

  }

  appointment_statistics() {
    console.log("newparams = " + this.newparams);
    this.myjsonBody = JSON.parse(this.newparams);

    // console.log("myjsonBody = "+this.myjsonBody);
    this.data.appointment_statistics(this.myjsonBody)
      .then(result => {
        // console.log(result);

        // var jsonBody = result["_body"];
        // jsonBody = JSON.parse(jsonBody);
        // this.appointment_statistics1 = jsonBody

        // console.log("Jsson body " + jsonBody);
      });

  }


  medical() {

    if (this.requester_id == null) {

      let alert = this.alertCtrl.create({
        title: '',
        subTitle: NOT_PERMITTED,
        buttons: ['OK']
      });
      alert.present();
    }

    else {
      // console.log("moving to medical");
      // this.showvalues();
      this.navCtrl.push(Search2Page, { value: this.from_login, doc_value: this.from_login_doc, pers_value: this.from_login_pers });

      // let modal = this.modalCtrl.create(SearchPage, {

      //   value: this.from_login, doc_value: this.from_login_doc, pers_value: this.from_login_pers
      // });
      // modal.present();
    }
  }

  // showvalues(){
  //   console.log("this.from_login = "+ this.from_login);
  //   console.log("this.from_login_doc = "+ this.from_login_doc);
  //   console.log("this.from_login_pers = "+ this.from_login_pers);
  // }


  medication() {

    if (this.requester_id == null) {

      let alert = this.alertCtrl.create({
        title: '',
        subTitle: NOT_PERMITTED,
        buttons: ['OK']
      });
      alert.present();
    }

    else {

      this.navCtrl.push(MedicationhistoryPage, { value: this.from_login, doc_value: this.from_login_doc, pers_value: this.from_login_pers });
      // let modal = this.modalCtrl.create(MedicationSearchPage, {

      //   value: this.from_login, doc_value: this.from_login_doc, pers_value: this.from_login_pers
      // });
      // modal.present();

    }
  }



  // this.navCtrl.push(MedAppointPage, { value: this.from_login,doc_value: this.from_login_doc,pers_value: this.from_login_pers });




  lab_history() {

    if (this.requester_id == null) {

      let alert = this.alertCtrl.create({
        title: '',
        subTitle: NOT_PERMITTED,
        buttons: ['OK']
      });
      alert.present();
    }

    else {
      this.navCtrl.push(Labhistory1Page, { value: this.from_login, doc_value: this.from_login_doc, pers_value: this.from_login_pers });

      //   let modal = this.modalCtrl.create(LabSearchPage, {

      //     value: this.from_login, doc_value: this.from_login_doc, pers_value: this.from_login_pers
      //   });
      //   modal.present();
    }
  }
  // this.navCtrl.push(LabHistoryPage, { value: this.from_login,doc_value: this.from_login_doc,pers_value: this.from_login_pers});

  //}



  doc_history() {

    if (this.requester_id == null) {

      let alert = this.alertCtrl.create({
        title: '',
        subTitle: NOT_PERMITTED,
        buttons: ['OK']
      });
      alert.present();
    }

    if (this.doc_id == null && this.person_type_id == "D") {

      let alert = this.alertCtrl.create({
        title: '',
        subTitle: NOT_PERMITTED2,
        buttons: ['OK']
      });
      alert.present();
    }

    // if (this.doc_id != null) {
    //   console.log('pushing to personalwelpage');

    //   this.navCtrl.push(PersonalWelPage, { value: this.from_login, doc_value: this.from_login_doc, pers_value: this.from_login_pers, requester_id: this.requester_id });
    // }

    // if (this.requester_id != null && this.doc_id == null && this.person_type_id == "C") {
    if (this.requester_id != null && this.person_type_id == "C") {

      let loader = this.loadingCtrl.create({
        content: "Please wait ..."

      });

      loader.present();


      console.log("THIS IS THE REquester ID " + this.requester_id)

      this.params = {
        "requester_id": this.requester_id
      }

      console.log('PARAMETERS' + this.params);

      this.data.check_patient_pds_status(this.params).then((result) => {

        console.log("RESULTS IS " + result);

        var body = result["_body"];
        body = JSON.parse(body);
        this.check = body
        console.log("RESULTS IS " + this.check);
        this.string = JSON.stringify(this.check)
        console.log("LETS SEE THE STRING " + this.string)

        this.jsonBody = JSON.parse(this.string);
        // this.sub_id = this.jsonBody[0].resp_code
        console.log("this.jsonBody[0].resp_code " + this.jsonBody.resp_code)

        console.log('pushing to locationpage');

        // this.navCtrl.push(LocationPage, { value: this.from_login, doc_value: this.from_login_doc, pers_value: this.from_login_pers });
        if (this.jsonBody.resp_code == "555") {

          loader.dismiss();
          let alert = this.alertCtrl.create({
            title: "Ghinger",
            subTitle: this.jsonBody.resp_desc,
            buttons: ['OK']
          });
          alert.present();

        }
        else if (this.jsonBody.resp_code == "100") {

          loader.dismiss();
          // let modal = this.modalCtrl.create(LocationPage, {

          //   value: this.from_login, pers_value: this.from_login_pers, doc_value: this.from_login_doc, requester_id: this.requester_id

          // });

          // modal.present();

          this.navCtrl.push(LocationPage, {
            value: this.from_login, pers_value: this.from_login_pers, doc_value: this.from_login_doc, requester_id: this.requester_id

          });


        }
        else if (this.jsonBody.resp_code == "000") {
          //doctor's records found. proceed to personal doctor welcome page.
          loader.dismiss();
          if (this.jsonBody.doc_records) {
            console.log('pushing to personalwelpage');

            this.navCtrl.push(PersonalWelPage, { value: this.from_login, doc_value: this.jsonBody.doc_records, pers_value: this.from_login_pers, requester_id: this.requester_id });
          }
        }


      }, (err) => {

        console.log(err);
      });

    }
  }



  check_pds_status() {


    console.log("THIS IS THE REquester ID " + this.requester_id)

    this.params = {
      "requester_id": this.requester_id
    }

    console.log('PARAMETERS' + this.params);

    this.data.check_patient_pds_status(this.params).then((result) => {

      console.log("RESULTS IS " + result);

      var body = result["_body"];
      body = JSON.parse(body);
      this.check = body
      console.log("RESULTS IS " + this.check);
      this.string = JSON.stringify(this.check)
      console.log("LETS SEE THE STRING " + this.string)

      this.jsonBody = JSON.parse(this.string);
      // this.sub_id = this.jsonBody[0].resp_code
      console.log("this.jsonBody[0].resp_code " + this.jsonBody.resp_code)

    }, (err) => {

      console.log(err);
    });




  }



  video(appointmentType) {
    // console.log(" appointmentType = "+appointmentType);
    if (this.requester_id == null) {

      let alert = this.alertCtrl.create({
        title: '',
        subTitle: NOT_PERMITTED,
        buttons: ['OK']
      });
      alert.present();
    }

    else {
      this.navCtrl.push(VideoconsulthistoryPage, { value: this.from_login, doc_value: this.from_login_doc, pers_value: this.from_login_pers, appointmentType: appointmentType });
    }

  }


  home(appointmentType) {

    if (this.requester_id == null) {

      let alert = this.alertCtrl.create({
        title: '',
        subTitle: NOT_PERMITTED,
        buttons: ['OK']
      });
      alert.present();
    }

    else {
      this.navCtrl.push(HomecarehistoryPage, { value: this.from_login, doc_value: this.from_login_doc, pers_value: this.from_login_pers, appointmentType: appointmentType });

    }


  }



  refresh() {

    // this.from_login = this.navParams.get('value')
    // this.body = Array.of(this.from_login)
    // this.jsonBody = JSON.parse(this.body);
    // this.requester_id = this.jsonBody[0].id
    // this.username = this.jsonBody[0].username
    // this.check = this.jsonBody[0]

    // this.body = Array.of(this.from_login_pers)
    // this.jsonBody = JSON.parse(this.body);
    // this.doc_id = this.jsonBody[0].doctor_id
    // this.check = this.jsonBody[0]

    // console.log("USERNAME HERE IS " + this.username)
    // console.log('VALUE of requester IN Menu  IS ' + this.requester_id);
    // console.log('VALUE of DOCTOR ID IN Menu  IS ' + this.doc_id);

    // console.log("THIS IS THE USERNAME " + this.username)
    // //  console.log ("THIS IS THE JSON PARSED USERNAME " + this.parsed_name)


    // this.params = {

    //   "username": this.username

    // }

    // let loader = this.loadingCtrl.create({
    //   content: "Refreshing ...",
    // });

    // loader.present();



    // this.data.retrieve1(this.params).then((result) => {


    //   var body1 = result["_body"];
    //   body1 = JSON.parse(body1);

    //   this.retrieve1 = body1
    //   this.retrieve1 = JSON.stringify(body1)

    //   console.log('LETS SEE THE  BODY ' + body1);
    //   console.log('LETS SEE THE DATA RETRIEVED ' + this.retrieve1);

    //   this.body1 = Array.of(this.retrieve1)
    //   this.jsonBody1 = JSON.parse(this.body1);
    //   this.person_type1 = this.jsonBody1[0].user_type
    //   this.doctor_id2 = this.jsonBody1[0].id

    //   console.log('-----------------------------------------------------');
    //   console.log('VALUE of USER TYPE  IS ' + this.person_type1);
    //   console.log('VALUE of REGIS ID  IS ' + this.doctor_id2);
    //   console.log('-----------------------------------------------------');

    // });



    // this.data.retrieve(this.params).then((result) => {

    //   var body = result["_body"];
    //   body = JSON.parse(body);

    //   this.retrieve = body
    //   this.retrieve = JSON.stringify(body)

    //   console.log('-----------------------------------------------------');
    //   console.log('-----------------THIS IS A LOG IN RETRIEVAL------------------------------------');
    //   console.log('LETS SEE THE  BODY ' + body);
    //   console.log('LETS SEE THE DATA RETRIEVED ' + this.retrieve);
    //   console.log('-----------------------------------------------------');

    //   this.body = Array.of(this.retrieve)
    //   this.jsonBody = JSON.parse(this.body);
    //   this.person_type = this.jsonBody[0].person_type_id
    //   this.requester_id = this.jsonBody[0].id



    //   console.log('VALUE of PERSON TYPE  IS ' + this.person_type);
    //   console.log('VALUE of requester IN Menu  IS ' + this.requester_id);

    // });

    // this.data.retrieve_pers(this.params).then((result) => {


    //   var body = result["_body"];
    //   body = JSON.parse(body);

    //   this.from_login_pers = body
    //   this.from_login_pers = JSON.stringify(body)

    //   console.log('LETS SEE THE  BODY ' + body);
    //   console.log('LETS SEE THE PERSON INFO DATA RETRIEVED ' + this.from_login_pers);

    //   this.body = Array.of(this.from_login_pers)
    //   this.jsonBody = JSON.parse(this.body);
    //   this.doc_id = this.jsonBody[0].doctor_id


    //   console.log('VALUE of DOCTOR ID IS ' + this.doc_id);

    //   this.doc_id1 = JSON.stringify(this.doc_id);
    //   this.jsonBody1 = JSON.parse(this.doc_id1);
    //   console.log("THIS IS THE JSON VALUES " + this.jsonBody1)


    //   this.doc_params = {

    //     "id": this.doctor_id2

    //   }

    //   this.doc_params2 = {

    //     "id": this.doc_id

    //   }
    //   console.log("------------DOC PARAMS----------");
    //   console.log(this.doc_params);


    //   this.data.retrieve_doc(this.doc_params).then((result) => {

    //     var body = result["_body"];
    //     body = JSON.parse(body);

    //     this.retrieve_doc = body
    //     this.retrieve_doc = JSON.stringify(body)
    //     console.log('LETS SEE THE DOCTOR INFO DATA RETRIEVED ' + this.retrieve_doc);


    //   });



    //   this.data.retrieve_doc(this.doc_params2).then((result) => {

    //     var body = result["_body"];
    //     console.log("LETS SEE BODY " + body)
    //     if (body == "") {
    //       console.log("BODY IS EMPTY")
    //     }

    //     else {
    //       body = JSON.parse(body);

    //       this.from_login_doc = body
    //       this.from_login_doc = JSON.stringify(body)
    //       console.log('LETS SEE THE DOCTOR INFO DATA RETRIEVED ' + this.from_login_doc);
    //     }

    //   });



    // });
    // loader.dismiss();


  }


  getappointments_counts(data) {

    // this.events.publish('doc_total_new_appoint_counter:refreshpage');
    console.log(data);

    if (data) {

      setTimeout(() => {

        this.data.get_patients_appointments_count(data)
          .then(result => {
            console.log(result);
            var jsonBody = result["_body"];

            if (jsonBody) {
              jsonBody = JSON.parse(jsonBody);

              if (jsonBody["medical_appointment_count"]) {
                this.medical_appointment_count = jsonBody["medical_appointment_count"];

                console.log(JSON.stringify(this.medical_appointment_count));
                this.storage.set("medical_appointment_count", JSON.stringify(this.medical_appointment_count));

              } else {
                this.medical_appointment_count = 0;
                this.storage.set("medical_appointment_count", "");
              }

              if (jsonBody["medication_appointment_count"]) {
                this.medication_appointment_count = jsonBody["medication_appointment_count"];
              } else {
                this.medication_appointment_count = 0;
              }

              if (jsonBody["labrequest_appointment_count"]) {
                this.labrequest_appointment_count = jsonBody["labrequest_appointment_count"];
              } else {
                this.labrequest_appointment_count = 0;
              }

              if (jsonBody["total_patient_pds_appointment_count"]) {
                this.total_patient_pds_appointment_count = jsonBody["total_patient_pds_appointment_count"];
              } else {
                this.total_patient_pds_appointment_count = 0;
              }

              if (jsonBody["videoconsult_appointment_count"]) {
                this.videoconsult_appointment_count = jsonBody["videoconsult_appointment_count"];
              } else {
                this.videoconsult_appointment_count = 0;
              }

              if (jsonBody["homecare_appointment_count"]) {
                this.homecare_appointment_count = jsonBody["homecare_appointment_count"];
              } else {
                this.homecare_appointment_count = 0;
              }



            }

            console.log("Jsson body " + jsonBody);
          }, (err) => {

            console.log("error = " + JSON.stringify(err));
          });

      }, 1);
    }

  }


}
